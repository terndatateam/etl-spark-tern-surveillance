import codecs
import os
import time
import traceback

from pyspark import SparkContext
from pyspark.sql import SparkSession
from spark_etl_utils import ListAccumulator, parse_vocab_from_url
from spark_etl_utils.citation.geonetwork_citation import (
    fix_namespace,
    get_party_from_metadata_record,
)
from spark_etl_utils.database import get_db_query_pg
from spark_etl_utils.rdf.vocabulary_mappings import upload_vocabulary_mappings
from tern_rdf import AUSPLOTS_BINDINGS, TernRdf

from config import Config
from transform_tables.common import create_taxonomy_index, load_ontology, perform_transform


def match_taxanomy_api(spark):
    db_host = os.getenv("POSTGRES_HOST")
    db_port = os.getenv("POSTGRES_PORT")
    db_name = os.getenv("POSTGRES_DB")
    db_username = os.getenv("POSTGRES_USER")
    db_password = os.getenv("POSTGRES_PASSWORD")
    query = """
          ( select distinct 
                hd.herbarium_determination as scientificName
            from ausplots.herbarium_determination hd
            where hd.herbarium_determination != 'Dead Tree/Shrub'
                and hd.herbarium_determination not like '%Annual%'
                and hd.herbarium_determination not like 'Unidentifiable sp.'
                and hd.herbarium_determination not like 'No Id'
                and hd.herbarium_determination not like 'No ID'
                and hd.herbarium_determination not like 'NO ID'
                and hd.herbarium_determination not like 'Fungi'
                and hd.herbarium_determination not like 'Fungi sp.'
                and hd.herbarium_determination not like 'Lichen'
                and hd.herbarium_determination not like 'Lichen sp.'
                and hd.herbarium_determination not like 'Cactus sp.'
                and hd.herbarium_determination not like 'Fungus sp.'
                and hd.herbarium_determination not like 'Moss'
                and hd.herbarium_determination not like 'delete'
          ) q
        """

    df = get_db_query_pg(
        spark,
        db_host,
        db_port,
        db_name,
        query,
        db_username,
        db_password,
    )

    df.toPandas().to_csv("temp.csv", sep=",", header=True, index=False)
    import requests

    url = "https://api-dev.tern.org.au/taxon/v1/match.csv"

    # payload = "scientificName,kingdom\r\nHedypnois rhagadioloides,Plantae\r\nCressa cretica,Plantae\r\nRhodanthe chlorocephaia var rosea,Plantae"
    headers = {"Content-Type": "text/csv"}
    with open("temp.csv", "r") as f:
        response = requests.request("POST", url, headers=headers, data=f.read(), verify=False)
        # with open(f"{Config.HERBARIUM_FILES}/output_species.csv", "w") as f:
        with codecs.open(f"{Config.HERBARIUM_FILES}/output_species.csv", "w", encoding="utf8") as f:
            f.write(response.text)

    create_taxonomy_index()


if __name__ == "__main__":
    start_time = time.time()

    FAILURES = []

    get_party_from_metadata_record(
        Config.DATASET,
        fix_namespace(Config.DATASET_NAMESPACE),
        Config.METADATA_URI,
        Config.OUTPUT_DIR,
        Config.OUTPUT_RDF_FORMAT,
    )

    try:
        sc = SparkContext()
        spark = (
            SparkSession.builder.master(Config.APP_MASTER).appName(Config.APP_NAME).getOrCreate()
        )

        # match_taxanomy_api(spark)
        # time.sleep(1000000)

        TABLES = Config.tables

        if Config.RELOAD_MAPPINGS:
            upload_vocabulary_mappings(
                spark, Config.MAPPING_CSV, Config.DATASET, "r_vocabulary_mappings", mode="overwrite"
            )

        # Get and broadcast the Controlled Vocabularies for the dataset
        vocabs_g = spark.sparkContext.broadcast(
            parse_vocab_from_url(
                parse_vocab_from_url(TernRdf.Graph([AUSPLOTS_BINDINGS]), Config.VOCABS[0]),
                Config.VOCABS[1],
            )
        )

        ontology_graph = load_ontology(Config.ONTOLOGIES)
        ontology = spark.sparkContext.broadcast(ontology_graph)

        for table_name in TABLES:
            errors = spark.sparkContext.accumulator([], ListAccumulator())
            warnings = spark.sparkContext.accumulator([], ListAccumulator())

            print(
                "-- {} ----------------------------------------------------------------------------------".format(
                    table_name
                )
            )
            table_start_time = time.time()

            try:
                mod = __import__("transform_tables.{}".format(table_name), fromlist=["Table"])
                table = mod.Table(spark)
                table_transform = table.transform
                table.clean()
                table.validate()
                table.pre_transform(errors, warnings)
                vocab_mappings = table.load_vocabulary_mappings()
                lookup = table.load_lookup()

                if hasattr(table, "perform_transform"):
                    if not table.perform_transform:
                        print(
                            "{} transform job finished in {:.2f} seconds".format(
                                table_name, time.time() - table_start_time
                            )
                        )
                        print("-------------\n")
                        continue

                perform_transform(
                    table,
                    table_transform,
                    table.dataset,
                    table.namespace,
                    table_name,
                    vocab_mappings,
                    vocabs_g,
                    errors,
                    warnings,
                    lookup,
                    ontology,
                )
                if len(errors.value) > 0:
                    FAILURES.append(table_name)
            except Exception as e:
                traceback.print_exception(type(e), e, e.__traceback__)
                FAILURES.append(table_name)

            print(
                "{} transform job finished in {:.2f} seconds".format(
                    table_name, time.time() - table_start_time
                )
            )
            print("-------------\n")

        print(
            "\n-- Output ----------------------------------------------------------------------------------"
        )
        print("Total Spark job finished in {:.2f} seconds".format(time.time() - start_time))

    except Exception as e:
        traceback.print_exception(type(e), e, e.__traceback__)
        FAILURES.append("Error initialising ETL")

    if FAILURES:
        with open(os.path.join(Config.APP_DIR, "FAILURE"), "w") as fp:
            pass
        print("Failed tables: {}".format(FAILURES))
    else:
        # Creating a file at specified location
        with open(os.path.join(Config.APP_DIR, "SUCCESS"), "w") as fp:
            pass
        print("No failures.")
