#!/bin/sh

if [ $# -eq 2 ]
then
  podman restart virtuoso \
  && sleep 60 \
  && podman exec virtuoso /usr/local/virtuoso-opensource/bin/isql-v verbose=off "EXEC=log_enable(2)" \
  && podman exec virtuoso /usr/local/virtuoso-opensource/bin/isql-v verbose=off "EXEC=UPDATE DB.DBA.RDF_QUAD TABLE OPTION (index RDF_QUAD_GS) SET g = iri_to_id ('http://$2')  WHERE g = iri_to_id ('http://$1', 0)" \
  && podman exec virtuoso /usr/local/virtuoso-opensource/bin/isql-v "EXEC=checkpoint" \
  && echo "renaming finished" > RENAME_FINISHED \
  && podman restart virtuoso \
  && sleep 10 \

else
    echo "Invalid number of arguments! Usage:"
    echo "sh rename_graph_virtuoso.sh CURRENT_GRAPH_NAME NEW_GRAPH_NAME (names wihtout http://)"
fi  
