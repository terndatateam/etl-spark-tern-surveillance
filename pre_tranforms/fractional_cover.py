import time

import rpy2.robjects as robjects
# import rpy2's package module
import rpy2.robjects.packages as rpackages
# import pandas as pd
from rpy2.robjects import pandas2ri
from rpy2.robjects.conversion import localconverter
# R vector of strings
from rpy2.robjects.vectors import StrVector


def fractional_cover_R(vegetation_df):
    # import R's utility package
    utils = rpackages.importr('utils')

    # select a mirror for R packages
    utils.chooseCRANmirror(ind=1)  # select the first mirror in the list

    # R package names
    packnames = ('plyr', 'simba')

    # Selectively install what needs to be install.
    # We are fancy, just because we can.
    names_to_install = [x for x in packnames if not rpackages.isinstalled(x)]
    if len(names_to_install) > 0:
        utils.install_packages(StrVector(names_to_install))

    # importr('plyr')

    robjects.r['source']("pre_tranforms/fractional_cover.R")
    fractional_cover_f = robjects.r['fractional_cover']

    start_time = time.time()

    with localconverter(robjects.default_converter + pandas2ri.converter):
        r_from_pd_df = robjects.conversion.py2rpy(vegetation_df)

    print('Converted from pandas df to R df in {:.2f} seconds'.format(time.time() - start_time))

    start_time = time.time()
    res_r_df = fractional_cover_f(r_from_pd_df, ground_fractional=True)

    with localconverter(robjects.default_converter + pandas2ri.converter):
        pd_from_r_df = robjects.conversion.rpy2py(res_r_df)

    pd_from_r_df['site_location_visit_id'] = pd_from_r_df.apply(lambda row: row.site_unique.split("-")[1], axis=1)
    print('Fractional cover in {:.2f} seconds'.format(time.time() - start_time))

    return pd_from_r_df
