import os
import time

from pyshacl import validate
from spark_etl_utils import parse_vocab_from_url
from tern_rdf import TernRdf, AUSPLOTS_BINDINGS

from config import Config

SHAQCL_FILE = "https://raw.githubusercontent.com/ternaustralia/ontology_tern-plot/master/docs/plot.shapes.ttl"


def shacl_validator(files_path: str, shacl_shape: str, vocabs: str):
    start_time = time.time()

    g = TernRdf.Graph([AUSPLOTS_BINDINGS])
    files = os.listdir(files_path)
    for file in files:
        print("Loading file '{}'".format(file))
        g.parse(location=files_path + "\\" + file, format="turtle")

    print('Graph loaded into Memory in {:.2f} seconds'.format(time.time() - start_time))
    start_time = time.time()

    shacl_shape_graph = parse_vocab_from_url(TernRdf.Graph([AUSPLOTS_BINDINGS]), shacl_shape)
    conforms, results_graph, results_text = validate(g, shacl_graph=shacl_shape_graph, ont_graph=vocabs,
                                                     inference='none', abort_on_error=False,
                                                     meta_shacl=False, debug=False, data_graph_format='ttl',
                                                     shacl_graph_format='ttl')

    print(results_text)

    print('Graph validated in {:.2f} seconds'.format(time.time() - start_time))

    # if not conforms:
    #     add_error(errors, "SHACL validation was unsuccessful for table: '{}'\n{}".format(table_name, results_text))
    return conforms


if __name__ == '__main__':
    vocab = parse_vocab_from_url(TernRdf.Graph([AUSPLOTS_BINDINGS]), Config.VOCABS[0])
    path = os.path.join("C:\\Users\\uqjsanc2\\PycharmProjects\\etl-spark-ausplots\\", Config.OUTPUT_DIR)
    shacl_validator(path, SHAQCL_FILE, vocab)
