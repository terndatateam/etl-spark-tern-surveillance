plot_ontology_shacl = """

@prefix ausplots: <http://linked.data.gov.au/dataset/ausplots/> .
@prefix ausplots-cv: <http://linked.data.gov.au/def/ausplots-cv/> .
@prefix biod: <http://linked.data.gov.au/def/biodiversity/> .
@prefix corveg-def: <http://linked.data.gov.au/def/corveg/> .
@prefix bioreg: <http://linked.data.gov.au/dataset/bioregion/> .
@prefix data: <http://linked.data.gov.au/def/datatype/> .
#@prefix dc: <http://purl.org/dc/elements/1.1/> .
@prefix dct: <http://purl.org/dc/terms/> .
@prefix dwct: <http://rs.tdwg.org/dwc/terms/> .
@prefix epsg-crs: <http://www.opengis.net/def/crs/epsg/0/> .
@prefix geo: <http://www.w3.org/2003/01/geo/wgs84_pos#> .
@prefix geosparql: <http://www.opengis.net/ont/geosparql#> .
@prefix geosparql-ext: <http://linked.data.gov.au/def/geox#> .
@prefix ldp: <http://www.w3.org/ns/ldp#> .
@prefix locn: <http://www.w3.org/ns/locn#> .
@prefix owl: <http://www.w3.org/2002/07/owl#> .
@prefix plot: <http://linked.data.gov.au/def/plot/> .
@prefix plot-x: <http://linked.data.gov.au/def/plot/x/> .
@prefix prov: <http://www.w3.org/ns/prov#> .
@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .
@prefix reg: <http://purl.org/linked-data/registry/> .
@prefix sdo: <http://schema.org/> .
@prefix skos: <http://www.w3.org/2004/02/skos/core#> .
@prefix sosa: <http://www.w3.org/ns/sosa/> .
@prefix ssn: <http://www.w3.org/ns/ssn/> .
@prefix ssn-ext: <http://www.w3.org/ns/ssn/ext/> .
@prefix time: <http://www.w3.org/2006/time#> .
@prefix ui: <http://purl.org/linked-data/registry-ui#> .
@prefix unit: <http://qudt.org/vocab/unit/> .
@prefix void: <http://rdfs.org/ns/void#> .
@prefix xml: <http://www.w3.org/XML/1998/namespace> .
@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .
@prefix sh: <http://www.w3.org/ns/shacl#> .
@prefix tern-ssn: <https://w3id.org/tern/ontologies/ssn/> .

ausplots:SiteShape
	a sh:NodeShape ;
	rdfs:label "Site Shape" ;
	sh:closed true ;
	sh:ignoredProperties ( rdf:type ) ;
	sh:ignoredProperties ( plot-x:sampleType ) ;
	sh:ignoredProperties ( plot-x:sampleLevel ) ;
	sh:ignoredProperties ( plot-x:floristics ) ;
	sh:ignoredProperties ( plot:mapScale ) ;
	sh:ignoredProperties ( locn:geographicName ) ;
	sh:ignoredProperties ( plot:mapsheetNumber ) ;
	sh:ignoredProperties ( plot:mapsheetName ) ;
	sh:ignoredProperties ( plot:locationMethod ) ;
	sh:property [
		a sh:PropertyShape ;
		sh:name "Label" ;
		sh:path rdfs:label ;
		sh:maxCount 1 ;
		sh:minCount 1 ;
		sh:nodeKind sh:Literal ;
		sh:datatype xsd:string ;
	] ;
	sh:property [
		a sh:PropertyShape ;
		sh:name "Identifier" ;
		sh:path dct:identifier ;
		sh:maxCount 1 ;
		sh:minCount 1 ;
		sh:nodeKind sh:Literal ;
		sh:or (  
		    [ sh:datatype xsd:integer ] 
		    [ sh:datatype xsd:string ] 
		) ;
	] ;
	sh:property [
		a sh:PropertyShape ;
		sh:name "Description" ;
		sh:path dct:description ;
		sh:maxCount 1 ;
		sh:nodeKind sh:Literal;
		sh:datatype xsd:string ;
	] ;
	sh:property [
		a sh:PropertyShape ;
		sh:name "Comment" ;
		sh:path rdfs:comment ;
		sh:maxCount 1 ;
		sh:nodeKind sh:Literal ;
		sh:datatype xsd:string ;
	] ;
	sh:property [
		a sh:PropertyShape ;
		sh:name "UFoI" ;
		sh:path sosa:hasUltimateFeatureOfInterest ; # IBRA
		sh:nodeKind sh:IRI ;
	] ;
	sh:property [
		a sh:PropertyShape ;
		sh:name "Part (subsite) of another Site" ;
		sh:path dct:isPartOf ;
		sh:maxCount 1 ;
		sh:nodeKind sh:IRI ;
	] ;
	sh:property [
		a sh:PropertyShape ;
		sh:name "Samples of the Site" ;
		sh:path sosa:hasSample ;
		sh:nodeKind sh:IRI ;
	] ;
	sh:property [
		a sh:PropertyShape ;
		sh:name "Date of establishment" ;
		sh:path prov:generatedAtTime ;
		sh:maxCount 1 ;
		sh:datatype xsd:dateTime ;
	] ;
    sh:property [
        a sh:PropertyShape ;
        sh:name "Primary Point (Lat, Long, Alt)" ;
        sh:path geosparql:hasGeometry ;
        sh:nodeKind sh:BlankNode ;
    ] ;
    sh:property [
		a sh:PropertyShape ;
		sh:name "Link to SiteVisit" ;
		sh:path plot:siteVisit ;
		sh:maxCount 1 ;
		sh:nodeKind sh:IRI;
	] ;
    sh:targetClass plot:Site ;
.

ausplots:SiteVisitShape
	a sh:NodeShape ;
	rdfs:label "Site Visit Shape" ;
	sh:closed true ;
	sh:ignoredProperties ( rdf:type ) ;
	sh:property [
		a sh:PropertyShape ;
		sh:name "Label" ;
		sh:path rdfs:label ;
		sh:maxCount 1 ;
		sh:minCount 1 ;
		sh:nodeKind sh:Literal ;
		sh:datatype xsd:string ;
	] ;
	sh:property [
		a sh:PropertyShape ;
		sh:name "Identifier" ;
		sh:path dct:identifier ;
		sh:maxCount 1 ;
		sh:minCount 1 ;
		sh:nodeKind sh:Literal ;
		sh:or (  [ sh:datatype xsd:integer ] [ sh:datatype xsd:string ] ) ;
	] ;
	sh:property [
		a sh:PropertyShape ;
		sh:name "Description" ;
		sh:path dct:description ;
		sh:maxCount 1 ;
		sh:nodeKind sh:Literal ;
		sh:datatype xsd:string ;
	] ;
	sh:property [
		a sh:PropertyShape ;
		sh:name "Comment" ;
		sh:path rdfs:comment ;
		sh:maxCount 1 ;
		sh:nodeKind sh:Literal ;
		sh:datatype xsd:string ;
	] ;
	sh:property [
		a sh:PropertyShape ;
		sh:name "Link to Site" ;
		sh:path plot:site ;
		sh:maxCount 1 ;
		sh:nodeKind sh:IRI;
	] ;
	# sh:property [
	# 	a sh:PropertyShape ;
	# 	sh:name "hasFeatureOfInterest for the Observation Collection" ;
	# 	sh:path sosa:hasFeatureOfInterest ;
	# 	sh:maxCount 1 ;
	# 	sh:minCount 1 ;
	# 	sh:nodeKind sh:BlankNode ;
	# 	sh:node [
	# 		sh:property [
	# 			sh:path sosa:isSampleOf ;
	# 			sh:nodeKind sh:IRI;
	# 			sh:minCount 1 ;
	# 			sh:maxCount 1 ;
	# 		] ;
	# 	] ;
	# ] ;
	sh:property [
		a sh:PropertyShape ;
		sh:name "Visit end time" ;
		sh:path prov:endedAtTime ;
		sh:maxCount 1 ;
		sh:minCount 1 ;
		sh:datatype xsd:dateTime ;
	] ;
	sh:property [
		a sh:PropertyShape ;
		sh:name "Visit start time" ;
		sh:path prov:startedAtTime ;
		sh:maxCount 1 ;
		sh:minCount 1 ;
		sh:datatype xsd:dateTime ;
	] ;
	sh:targetClass plot:SiteVisit ;
.

ausplots:CollectedSampleShape
	a sh:NodeShape ;
	rdfs:label "TERN SSN Collected Sample Shape" ;
	sh:closed true ;
	sh:ignoredProperties ( rdf:type ) ;
	sh:property [
		a sh:PropertyShape ;
		sh:name "Label" ;
		sh:path rdfs:label ;
		sh:maxCount 1 ;
		sh:minCount 1 ;
		sh:nodeKind sh:Literal ;
		sh:datatype xsd:string ;
	] ;
	sh:property [
		a sh:PropertyShape ;
		sh:name "Identifier" ;
		sh:path dct:identifier ;
		sh:maxCount 1 ;
		sh:nodeKind sh:Literal ;
		sh:or ( [ sh:datatype xsd:integer ] [ sh:datatype xsd:string ] ) ;
	] ;   
	sh:property [
		a sh:PropertyShape ;
		sh:name "Comment" ;
		sh:path rdfs:comment ;
		sh:maxCount 1 ;
		sh:nodeKind sh:Literal ;
		sh:datatype xsd:string ;
	] ;
	sh:property [
		a sh:PropertyShape ;
		sh:name "Sample name" ;
		sh:path sosa:hasResult ;
		sh:class data:Text ;
	] ;
	sh:property [
		a sh:PropertyShape ;
		sh:name "Sampling Method" ;
		sh:path sosa:isResultOf ;
		sh:nodeKind sh:BlankNode ;
		sh:class sosa:Sampling ;
		sh:node [
			sh:property [
				sh:name "Used procedure for sampling" ;
				sh:path sosa:usedProcedure ;
				sh:maxCount 1 ;
				sh:minCount 1 ;
				sh:nodeKind sh:BlankNode ;
		        sh:class sosa:Procedure ;
			]
		]
	] ;
	sh:property [
		a sh:PropertyShape ;
		sh:name "Is Subsample of another Sample" ;
		sh:path sosa:isSampleOf ;
		sh:nodeKind sh:IRI;
	] ;
	sh:property [
		a sh:PropertyShape ;
		sh:name "Has a Subsample" ;
		sh:path sosa:hasSample ;
		sh:nodeKind sh:IRI;
	] ;
	sh:property [
		a sh:PropertyShape ;
		sh:name "DWCT Name According To" ;
		sh:path dwct:nameAccordingTo ;
		sh:nodeKind sh:IRI ;
		sh:class dwct:Identification ;
	] ;
	sh:property [
		a sh:PropertyShape ;
		sh:name "DWCT Material Sample ID" ;
		sh:path dwct:materialSampleID ;
		sh:nodeKind sh:Literal ;
		sh:datatype xsd:string ;
	] ;
	sh:property [
		a sh:PropertyShape ;
		sh:name "Was attributed To" ;
		sh:path prov:wasAttributedTo ;
		sh:nodeKind sh:IRI;
		# sh:class sdo:Person etc.
	] ;	
	sh:property [
		a sh:PropertyShape ;
		sh:name "Link to SiteVisit" ;
		sh:path plot:siteVisit ;
		sh:maxCount 1 ;
		sh:nodeKind sh:IRI;
	] ;
	sh:targetClass tern-ssn:CollectedSample ;
.

ausplots:SampleShape
	a sh:NodeShape ;
	rdfs:label "Sample Shape" ;
	sh:closed true ;
	sh:ignoredProperties ( rdf:type ) ;
	sh:property [
		a sh:PropertyShape ;
		sh:name "Label" ;
		sh:path rdfs:label ;
		sh:maxCount 1 ;
		sh:minCount 1 ;
		sh:nodeKind sh:Literal ;
		sh:datatype xsd:string ;
	] ;
	sh:property [
		a sh:PropertyShape ;
		sh:name "Identifier" ;
		sh:path dct:identifier ;
		sh:maxCount 1 ;
		sh:nodeKind sh:Literal ;
		sh:or ( [ sh:datatype xsd:integer ] [ sh:datatype xsd:string ] ) ;
	] ;   
	sh:property [
		a sh:PropertyShape ;
		sh:name "Comment" ;
		sh:path rdfs:comment ;
		sh:maxCount 1 ;
		sh:nodeKind sh:Literal ;
		sh:datatype xsd:string ;
	] ;
	sh:property [
		a sh:PropertyShape ;
		sh:name "Sample name" ;
		sh:path sosa:hasResult ;
		sh:class data:Text ;
	] ;
	sh:property [
		a sh:PropertyShape ;
		sh:name "Sampling Method" ;
		sh:path sosa:isResultOf ;
		sh:nodeKind sh:BlankNode ;
		sh:class sosa:Sampling ;
		sh:node [
			sh:property [
				sh:name "Used procedure for sampling" ;
				sh:path sosa:usedProcedure ;
				sh:maxCount 1 ;
				sh:minCount 1 ;
				sh:nodeKind sh:BlankNode ;
		        sh:class sosa:Procedure ;
			]
		]
	] ;
	sh:property [
		a sh:PropertyShape ;
		sh:name "Is Subsample of another Sample" ;
		sh:path sosa:isSampleOf ;
		sh:nodeKind sh:IRI;
	] ;
	sh:property [
		a sh:PropertyShape ;
		sh:name "Has a Subsample" ;
		sh:path sosa:hasSample ;
		sh:nodeKind sh:IRI;
	] ;
	sh:property [
		a sh:PropertyShape ;
		sh:name "DWCT Name According To" ;
		sh:path dwct:nameAccordingTo ;
		sh:nodeKind sh:IRI ;
		sh:class dwct:Identification ;
	] ;
	sh:property [
		a sh:PropertyShape ;
		sh:name "DWCT Material Sample ID" ;
		sh:path dwct:materialSampleID ;
		sh:nodeKind sh:Literal ;
		sh:datatype xsd:string ;
	] ;
	sh:property [
		a sh:PropertyShape ;
		sh:name "Was attributed To" ;
		sh:path prov:wasAttributedTo ;
		sh:nodeKind sh:IRI;
		# sh:class sdo:Person etc.
	] ;
	sh:property [
		a sh:PropertyShape ;
		sh:name "Link to Site" ;
		sh:path plot:site ;
		sh:maxCount 1 ;
		sh:nodeKind sh:IRI;
	] ;
	sh:property [
		a sh:PropertyShape ;
		sh:name "Samples related to the observation, point intercept" ;
		sh:path sosa:hasFeatureOfInterest ; # Sample
		sh:nodeKind sh:IRI ;
	] ;
	sh:targetClass sosa:Sample ;
.

ausplots:DataTextShape
	a sh:NodeShape ;
	rdfs:label "Data Text Shape" ;
	sh:closed true ;
	sh:ignoredProperties ( rdf:type ) ;
	sh:property [
		a sh:PropertyShape ;
		sh:name "Literal Text value" ;
		sh:path data:value ;
		sh:maxCount 1 ;
		sh:minCount 1 ;
		sh:nodeKind sh:Literal;
		sh:datatype xsd:string ;
	] ;
	sh:targetClass data:Text ;
.

ausplots:DataQuantitativeMeasureShape
	a sh:NodeShape ;
	rdfs:label "Data Quantitative Measure Shape" ;
	sh:closed true ;
	sh:ignoredProperties ( rdf:type ) ;
	sh:property [
		a sh:PropertyShape ;
		sh:name "Data Quantitative Measure value" ;
		sh:path data:value ;
		sh:maxCount 1 ;
		#sh:minCount 1 ;
		sh:nodeKind sh:Literal;
		sh:or ( [ sh:datatype xsd:decimal ] [ sh:datatype xsd:integer ] [ sh:datatype xsd:string ] [ sh:datatype xsd:double ] ) ;
	] ;
	sh:property [
		a sh:PropertyShape ;
		sh:name "Data Quantitative Measure uncertainty unit" ;
		sh:path data:uncertainty ;
		sh:maxCount 1 ;
		sh:datatype xsd:integer ;
	] ;
	sh:property [
		a sh:PropertyShape ;
		sh:name "Data Quantitative Measure standard unit" ;
		sh:path data:unit ;
		sh:maxCount 1 ;
		sh:nodeKind sh:IRI;
	] ;
	sh:targetClass data:QuantitativeMeasure ;
.

ausplots:DataBooleanShape
	a sh:NodeShape ;
	rdfs:label "Data Boolean Shape" ;
	sh:closed true ;
	sh:ignoredProperties ( rdf:type ) ;
	sh:property [
		a sh:PropertyShape ;
		sh:name "Literal Boolean value" ;
		sh:path data:value ;
		sh:maxCount 1 ;
		sh:minCount 1 ;
		sh:nodeKind sh:Literal;
		sh:datatype xsd:boolean ;
	] ;
	sh:targetClass data:Boolean ;
.

ausplots:DataConceptShape
	a sh:NodeShape ;
	rdfs:label "Data Concept Shape" ;
	sh:closed true ;
	sh:ignoredProperties ( rdf:type ) ;
	sh:property [
		a sh:PropertyShape ;
		sh:name "Concept value" ;
		sh:path data:value ;
		sh:maxCount 1 ;
		sh:minCount 1 ;
		sh:nodeKind sh:IRI;
	] ;
	sh:targetClass data:Concept ;
.

ausplots:ObservedPropertyShape
	a sh:NodeShape ;
	rdfs:label "Observed Property Shape" ;
	sh:closed true ;
	sh:ignoredProperties ( rdf:type ) ;
	sh:property [
		a sh:PropertyShape ;
		sh:name "Observed Property" ;
		sh:path data:value ;
		sh:maxCount 1 ;
		sh:minCount 1 ;
		sh:nodeKind sh:IRI;
	] ;
	sh:property [
		a sh:PropertyShape ;
		sh:name "Data Qualifier" ;
		sh:path data:standard ;
		sh:nodeKind sh:IRI;
	] ;
	sh:targetClass sosa:ObservableProperty ;
.

# ausplots:UsedProcedureShape
# 	a sh:NodeShape ;
# 	rdfs:label "Used Procedure Shape" ;
# 	sh:closed true ;
# 	sh:ignoredProperties ( rdf:type ) ;
# 	sh:property [
# 		a sh:PropertyShape ;
# 		sh:name "Used procedure" ;
# 		sh:path data:value ;
# 		sh:maxCount 1 ;
# 		sh:minCount 0 ;
# 		sh:nodeKind sh:IRI;
# 	] ;
# 	sh:property [
# 		a sh:PropertyShape ;
# 		sh:name "Used procedure hasInput" ;
# 		sh:path ssn:hasInput ;
# 		sh:maxCount 1 ;
# 		sh:nodeKind sh:BlankNode;
# 		sh:class ssn:Input ;
# 	] ;
# 	sh:targetClass sosa:Procedure ;
# .

ausplots:ObservationShape
	a sh:NodeShape ;
	rdfs:label "Observation Shape" ;
	sh:closed true ;
	sh:ignoredProperties ( rdf:type ) ;
	sh:property [
		a sh:PropertyShape ;
		sh:name "Observation Label" ;
		sh:path rdfs:label ;
		sh:maxCount 1 ;
		sh:minCount 1 ;
		sh:nodeKind sh:Literal ;
		sh:datatype xsd:string ;
	] ;
	sh:property [
		a sh:PropertyShape ;
		sh:name "Type of the Collection" ;
		sh:path plot:observationTheme;
		sh:maxCount 2 ;
		sh:nodeKind sh:IRI;
	] ;
	sh:property [
		a sh:PropertyShape ;
		sh:name "site of the Observation Collection" ;
		sh:path plot:site ;
		sh:maxCount 1 ;
		sh:minCount 1 ;
		sh:nodeKind sh:IRI ;
	] ;
	sh:property [
		a sh:PropertyShape ;
		sh:name "Site Visit of the Observation" ;
		sh:path plot:wasSubActivityOf ; # SiteVisit
		sh:minCount 1 ;
        sh:maxCount 1 ;
		sh:nodeKind sh:IRI ;
	] ;
	sh:property [
		a sh:PropertyShape ;
		sh:name "Observation Identifier" ;
		sh:path dct:identifier ;
		sh:maxCount 1 ;
		sh:nodeKind sh:Literal ;
        sh:datatype xsd:integer ;
	] ;
    sh:property [
		a sh:PropertyShape ;
		sh:name "Samples related to the observation" ;
		sh:path sosa:hasFeatureOfInterest ; # Sample
		sh:nodeKind sh:IRI ;
	] ;   
    sh:property [
		a sh:PropertyShape ;
		sh:name "Used procedure for the observation" ;
		sh:path sosa:usedProcedure ;
		sh:maxCount 1 ;
		sh:minCount 1 ;
		sh:nodeKind sh:BlankNode ;
		sh:class sosa:Procedure ;
	] ;
    sh:property [
		a sh:PropertyShape ;
		sh:name "Observed property for the observation" ;
		sh:path sosa:observedProperty ;
		sh:maxCount 1 ;
		sh:minCount 1 ;
		sh:nodeKind sh:BlankNode ;
		sh:class sosa:ObservableProperty ;
	] ;
	sh:property [
		a sh:PropertyShape ;
		sh:name "SOSA Result Time" ;
		sh:path sosa:resultTime ;
		sh:datatype xsd:dateTime ;
	] ;
    sh:property [
		a sh:PropertyShape ;
		sh:name "Result of the observation" ;		
		sh:maxCount 1 ;
		sh:minCount 1 ;
		sh:path sosa:hasResult ;
		sh:nodeKind sh:BlankNode ;
		sh:or( 
		    [ sh:class data:Concept ] 
		    [ sh:class data:Boolean ] 
		    [ sh:class data:Text ] 
		    [ sh:class data:Count ] 
		    [ sh:class data:Percent ] 
		    [ sh:class data:QuantitativeMeasure ]  
		)
	] ;
	sh:property [
		a sh:PropertyShape ;
		sh:name "Phenomenom time of the Observation" ;
		sh:path sosa:phenomenonTime ;
		sh:maxCount 1 ;
		sh:minCount 1 ;
		sh:nodeKind sh:IRI;
	] ;
	# sh:property [
	# 	a sh:PropertyShape ;
	# 	sh:name "hasFeatureOfInterest for the observation" ;
	# 	sh:path sosa:hasFeatureOfInterest ;
	# 	sh:maxCount 1 ;
	# 	#sh:minCount 1 ;
	# 	sh:nodeKind sh:BlankNode ;
	# 	sh:node [
	# 		sh:property [
	# 			sh:path sosa:isSampleOf ;
	# 			sh:nodeKind sh:IRI;
	# 			sh:minCount 1 ;
	# 			sh:maxCount 1 ;
	# 		] ;
	# 	] ;
	# ] ;
	sh:targetClass sosa:Observation ;
.

ausplots:ObservationCollectionShape
	a sh:NodeShape ;
	rdfs:label "Observation Collection Shape" ;
	sh:closed true ;
	sh:ignoredProperties ( rdf:type ) ;
	sh:ignoredProperties ( plot:hasResultQuality ) ;
	sh:ignoredProperties ( plot-x:stemDensityArea ) ;
	sh:ignoredProperties ( sosa:isSampleOf ) ;
	sh:property [
		a sh:PropertyShape ;
		sh:name "Observation Collection Label" ;
		sh:path rdfs:label ;
		sh:maxCount 1 ;
		sh:minCount 1 ;
		sh:nodeKind sh:Literal ;
		sh:datatype xsd:string ;
	] ;
    sh:property [
		a sh:PropertyShape ;
		sh:name "Site Visit of the Collection" ;
		sh:path plot:wasSubActivityOf ; # SiteVisit
		sh:minCount 1 ;
        sh:maxCount 1 ;
		sh:nodeKind sh:IRI ;
	] ;
    sh:property [
		a sh:PropertyShape ;
		sh:name "Type of the Collection" ;
		sh:path plot:observationTheme;
		sh:maxCount 2 ;
		sh:nodeKind sh:IRI;
	] ;
	sh:property [
		a sh:PropertyShape ;
		sh:name "Samples of the Collection" ;
		sh:path sosa:hasSample ;
		sh:nodeKind sh:IRI ;
	] ;
    sh:property [
		a sh:PropertyShape ;
		sh:name "Members (observations) of the Collection" ;
		sh:path ssn-ext:hasMember ;
		sh:nodeKind sh:IRI ;
	] ;
	sh:property [
		a sh:PropertyShape ;
		sh:name "Creation Date" ;
		sh:path dct:created ;
		sh:maxCount 1 ;
		sh:nodeKind sh:BlankNode ;
		sh:class time:Instant ;
		sh:node [
			sh:property [
				sh:path time:inXSDDateTime ;
				sh:datatype xsd:dateTime ;
				sh:minCount 1 ;
				sh:maxCount 1 ;
			] ;
		] ;
	] ;
    sh:property [
		a sh:PropertyShape ;
		sh:name "DCT Description" ;
		sh:path dct:description ;
		sh:maxCount 1 ;
		sh:nodeKind sh:Literal;
		sh:datatype xsd:string ;
	] ;
	sh:property [
		a sh:PropertyShape ;
		sh:name "Coment" ;
		sh:path rdfs:comment ;
		sh:maxCount 1 ;
		sh:nodeKind sh:Literal ;
		sh:datatype xsd:string ;
	] ;
	sh:property [
		a sh:PropertyShape ;
		sh:name "hasFeatureOfInterest for the Observation Collection" ;
		sh:path plot:site ;
		sh:maxCount 1 ;
		sh:minCount 1 ;
		sh:nodeKind sh:IRI ;
	] ;
	sh:targetClass ssn-ext:ObservationCollection ;
.

ausplots:IdentificationShape
	a sh:NodeShape ;
	rdfs:label "DWCT Identification Shape" ;
	sh:closed true ;
	sh:ignoredProperties ( rdf:type ) ;
	sh:property [
		a sh:PropertyShape ;
		sh:name "DWCT Scientific Name" ;
		sh:path dwct:scientificName ;
		sh:maxCount 1 ;
		sh:minCount 1 ;
		sh:nodeKind sh:Literal ;
		sh:datatype xsd:string ;
	] ;
    sh:property [
		a sh:PropertyShape ;
		sh:name "DWCT Identified By" ;
		sh:path dwct:identifiedBy ;
		sh:maxCount 1 ;
		sh:nodeKind sh:Literal ;
		sh:datatype xsd:string ;
	] ;
	sh:property [
		a sh:PropertyShape ;
		sh:name "DWCT Date Identified" ;
		sh:path dwct:dateIdentified ;
		sh:maxCount 1 ;
		sh:nodeKind sh:Literal ;
		sh:datatype xsd:dateTime ;
	] ;
	sh:property [
		a sh:PropertyShape ;
		sh:name "DWCT Identification ID" ;
		sh:path dwct:identificationID ;
		sh:nodeKind sh:Literal ;
		sh:datatype xsd:string ;
	] ;
	sh:property [
		a sh:PropertyShape ;
		sh:name "DWCT Identification Remarks" ;
		sh:path dwct:identificationRemarks ;
		sh:nodeKind sh:Literal ;
		sh:datatype xsd:string ;
	] ;
    sh:property [
		a sh:PropertyShape ;
		sh:name "Close Match with Taxon" ;
		sh:path skos:closeMatch ;
		sh:maxCount 1 ;
		sh:nodeKind sh:IRI ;
	] ;
	sh:targetClass dwct:Identification ;
.

ausplots:TaxonShape
	a sh:NodeShape ;
	rdfs:label "DWCT Taxon Shape" ;
	sh:closed true ;
	sh:ignoredProperties ( rdf:type ) ;
	sh:property [
		a sh:PropertyShape ;
		sh:name "DWCT Accepted Name Usage" ;
		sh:path dwct:acceptedNameUsage ;
		sh:maxCount 1 ;
		sh:minCount 1 ;
		sh:nodeKind sh:Literal ;
		sh:datatype xsd:string ;
	] ;
	sh:property [
		a sh:PropertyShape ;
		sh:name "DWCT Accepted Name Usage ID" ;
		sh:path dwct:acceptedNameUsageID ;
		sh:maxCount 1 ;
		sh:minCount 1 ;
		sh:nodeKind sh:IRI ;
	] ;
    sh:property [
		a sh:PropertyShape ;
		sh:name "DWCT Class" ;
		sh:path dwct:class ;
		sh:maxCount 1 ;
		sh:minCount 1 ;
		sh:nodeKind sh:Literal ;
		sh:datatype xsd:string ;
	] ;
	sh:property [
		a sh:PropertyShape ;
		sh:name "DWCT Family" ;
		sh:path dwct:family ;
		sh:maxCount 1 ;
		sh:minCount 1 ;
		sh:nodeKind sh:Literal ;
		sh:datatype xsd:string ;
	] ;
	sh:property [
		a sh:PropertyShape ;
		sh:name "DWCT Higher Classification" ;
		sh:path dwct:higherClassification ;
		sh:maxCount 1 ;
		sh:minCount 1 ;
		sh:nodeKind sh:Literal ;
		sh:datatype xsd:string ;
	] ;
	sh:property [
		a sh:PropertyShape ;
		sh:name "DWCT Kingdom" ;
		sh:path dwct:kingdom ;
		sh:maxCount 1 ;
		sh:minCount 1 ;
		sh:nodeKind sh:Literal ;
		sh:datatype xsd:string ;
	] ;
	sh:property [
		a sh:PropertyShape ;
		sh:name "DWCT Name According To" ;
		sh:path dwct:nameAccordingTo ;
		sh:maxCount 1 ;
		sh:minCount 1 ;
		sh:nodeKind sh:Literal ;
		sh:datatype xsd:string ;
	] ;
	sh:property [
		a sh:PropertyShape ;
		sh:name "DWCT Name According To ID" ;
		sh:path dwct:nameAccordingToID ;
		sh:maxCount 1 ;
		sh:minCount 1 ;
		sh:nodeKind sh:IRI ;
	] ;
	sh:property [
		a sh:PropertyShape ;
		sh:name "DWCT Nomenclatural Code" ;
		sh:path dwct:nomenclaturalCode ;
		sh:maxCount 1 ;
		sh:minCount 1 ;
		sh:nodeKind sh:Literal ;
		sh:datatype xsd:string ;
	] ;
	sh:property [
		a sh:PropertyShape ;
		sh:name "DWCT Parent Name Usage ID" ;
		sh:path dwct:parentNameUsageID ;
		sh:maxCount 1 ;
		sh:minCount 1 ;
		sh:nodeKind sh:IRI ;
	] ;
	sh:property [
		a sh:PropertyShape ;
		sh:name "DWCT Scientific Name" ;
		sh:path dwct:scientificName ;
		sh:maxCount 1 ;
		sh:minCount 1 ;
		sh:nodeKind sh:Literal ;
		sh:datatype xsd:string ;
	] ;
	sh:property [
		a sh:PropertyShape ;
		sh:name "DWCT Scientific Name Authorship" ;
		sh:path dwct:scientificNameAuthorship ;
		sh:maxCount 1 ;
		sh:minCount 1 ;
		sh:nodeKind sh:Literal ;
		sh:datatype xsd:string ;
	] ;
	sh:property [
		a sh:PropertyShape ;
		sh:name "DWCT Scientific Name ID" ;
		sh:path dwct:scientificNameID ;
		sh:maxCount 1 ;
		sh:minCount 1 ;
		sh:nodeKind sh:IRI ;
	] ;
	sh:property [
		a sh:PropertyShape ;
		sh:name "DWCT Taxon Concept ID" ;
		sh:path dwct:taxonConceptID ;
		sh:maxCount 1 ;
		sh:minCount 1 ;
		sh:nodeKind sh:IRI ;
	] ;
	sh:property [
		a sh:PropertyShape ;
		sh:name "DWCT Taxon ID" ;
		sh:path dwct:taxonID ;
		sh:maxCount 1 ;
		sh:minCount 1 ;
		sh:nodeKind sh:IRI ;
	] ;
	sh:property [
		a sh:PropertyShape ;
		sh:name "DWCT Taxon Rank" ;
		sh:path dwct:taxonRank ;
		sh:maxCount 1 ;
		sh:minCount 1 ;
		sh:nodeKind sh:Literal ;
		sh:datatype xsd:string ;
	] ;
	sh:property [
		a sh:PropertyShape ;
		sh:name "DWCT Taxonomic Status" ;
		sh:path dwct:taxonomicStatus ;
		sh:maxCount 1 ;
		sh:minCount 1 ;
		sh:nodeKind sh:Literal ;
		sh:datatype xsd:string ;
	] ;
	sh:property [
		a sh:PropertyShape ;
		sh:name "SKOS Alternative Label" ;
		sh:path skos:altLabel ;
		sh:maxCount 1 ;
		sh:minCount 1 ;
		sh:nodeKind sh:Literal ;
		sh:datatype xsd:string ;
	] ;
	sh:property [
		a sh:PropertyShape ;
		sh:name "SKOS Preferred Label" ;
		sh:path skos:prefLabel ;
		sh:maxCount 1 ;
		sh:minCount 1 ;
		sh:nodeKind sh:Literal ;
		sh:datatype xsd:string ;
	] ;
	sh:property [
		a sh:PropertyShape ;
		sh:name "SKOS In Scheme" ;
		sh:path skos:inScheme ;
		sh:maxCount 1 ;
		sh:minCount 1 ;
		sh:nodeKind sh:IRI ;
	] ;
	sh:property [
		a sh:PropertyShape ;
		sh:name "DCT Modified Date" ;
		sh:path dct:modified ;
		sh:maxCount 1 ;
		sh:minCount 1 ;
		sh:nodeKind sh:Literal ;
		sh:datatype xsd:dateTime ;
	] ;
	sh:property [
		a sh:PropertyShape ;
		sh:name "DCT Created Date" ;
		sh:path dct:created ;
		sh:maxCount 1 ;
		sh:minCount 1 ;
		sh:minCount 1 ;
		sh:nodeKind sh:Literal ;
		sh:datatype xsd:dateTime ;
	] ;
	sh:targetClass dwct:Taxon ;
.

"""
