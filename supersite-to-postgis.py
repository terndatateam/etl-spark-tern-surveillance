import pandas as pd
import psycopg2 as pg
from psycopg2._psycopg import AsIs

# df = pd.read_csv("supersites.csv")

# # with open("supersite_postgis.sql", "w") as f:
# i = 1
# for index, row in df.iterrows():
#     # f.write(f"""INSERT INTO supersites ("grid_code","uri","supersite_name",geom) VALUES ({i},'{row["supersite"]}','{row["id"]}',ST_GeomFromText('{row["polygon"]}',3577));""")
#     print(f"""INSERT INTO supersites ("grid_code","uri","supersite_name","supersite_type",geom) VALUES ({i},'{row["supersite"]}','{row["id"]}','{row["siteType"]}',ST_GeomFromText('{row["polygon"]}',3577));""")
#     i += 1


df = pd.read_csv("C:\\sites.csv")

conn = pg.connect(host="localhost", port="9999", dbname="ultimate_feature_of_interest",
                  user="postgres", password="postgres")

src_cursor = conn.cursor()

for index, row in df.iterrows():
    src_cursor.execute(
        "select uri, supersite_name, supersite_type from supersites where ST_Intersects(geom, ST_SetSRID(ST_MakePoint(%(lon)s, %(lat)s), 3577))",
        {'lon': row["point_longitude"], 'lat': row["point_latitude"]})
    result = src_cursor.fetchone()
    if result is None:
        print(row["persistent_uri"])

src_cursor.close()