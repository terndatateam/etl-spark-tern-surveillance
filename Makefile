build-ingest:
	docker-compose build ingest

bash-ingest:
	docker-compose run ingest bash

run-db:
	docker-compose up -d db

run-spark:
	spark-submit --driver-class-path postgresql-42.2.10.jar --conf spark.driver.memory=2g --conf spark.executor.memory=2g etl.py > output.log