import os
import numexpr
from tern_rdf.namespace_bindings import AUSPLOTS_DATASET

try:
    from dotenv import load_dotenv

    load_dotenv()
except:
    pass


class Config:
    APP_DIR = os.path.dirname(os.path.realpath(__file__))
    OUTPUT_DIR = "output"
    OUTPUT_RDF_FORMAT = "turtle"

    # Sets a name for the application, which will be shown in the Spark web UI.
    # If no application name is set, a randomly generated name will be used.
    APP_NAME = "AUSPLOTS SPARK-ETL"

    DATASET = "ausplots"
    DATASET_NAMESPACE = AUSPLOTS_DATASET

    METADATA_URI = "9210c331-ba96-4e0c-9554-f029d61534be"

    # Sets the Spark master URL to connect to, such as "local" to run locally, "local[4]" to run locally with 4 cores,
    # or "spark://master:7077" to run on a Spark standalone cluster.
    CPU_COUNT = numexpr.detect_number_of_cores()
    print(f"Executing ETL with {CPU_COUNT} cores/threads")
    APP_MASTER = f"local[{CPU_COUNT}]"

    # Select the tables to process commenting and uncommenting them
    tables = [
        "site_location",
        "site_location_point",
        "site_location_visit",
        "basal_area",
        "characterisation_coarse_frags_abundance",
        "characterisation_coarse_frags_lithology",
        "characterisation_coarse_frags_shape",
        "characterisation_coarse_frags_size",
        "herbarium_determination",
        "point_intercept",
        "tern_point_intercept_fungi",
        "soil_bulk_density",
        "soil_characterisation",
        "structural_summary",
        "veg_vouchers",
        "visit_surface_coarse_frags_abundance",
        "visit_surface_coarse_frags_lithology",
        "visit_surface_coarse_frags_type",
        "visit_surface_coarse_frags_size",
        "visit_surface_soil_conditions",
        # "tern_climate_data", # disabled
        "tern_weather_data",
        "tern_derived_ground_cover",
        # "tern_derived_species_individuals_and_abundance",  # disabled
        # "tern_derived_species_richness",  # disabled
    ]

    ufoi_list = {
        "local_government_areas_2011": [
            "http://linked.data.gov.au/dataset/local-gov-areas-2011/",
            "lga_code11",
        ],
        "capad_2018_terrestrial": [
            "http://linked.data.gov.au/dataset/capad-2018-terrestrial/",
            "pa_pid",
        ],
        "ibra7_regions": ["http://linked.data.gov.au/dataset/bioregion/", "reg_code_7"],
        "ibra7_subregions": ["http://linked.data.gov.au/dataset/bioregion/", "sub_code_7"],
        "nrm_regions": ["http://linked.data.gov.au/dataset/nrm-2017/", "nrm_id"],
        "states_territories": [
            "http://linked.data.gov.au/dataset/asgs2016/stateorterritory/",
            "state_code",
        ],
        "wwf_terr_ecoregions": [
            "http://linked.data.gov.au/dataset/wwf-terr-ecoregions/",
            "objectid",
        ],
    }

    # Force the execution to re-download the Taxa files and re-index them.
    # This may take a long time
    force_taxa_index = False
    force_taxanomy_index = False

    # APNI and APC urls (for taxa tables)
    APNI = "http://linked.data.gov.au/dataset/apni"
    APC = "http://linked.data.gov.au/dataset/apc"

    GENERALISATION_REPORT = "https://sds.ala.org.au/sensitive-species-data.xml"
    CONSERVATION_STATUS_LIST = os.path.join(APP_DIR, "conservation_status_report_11102022.csv")

    # Choose the currently accepted national taxonomy needed for the project
    taxas = [
        "APNI/APC",
        "AusMoss",
        # "Fungi",
        # "Lichen"
    ]

    HERBARIUM_FILES = "taxa_files"

    # Source for vocabularies and ontologies
    VOCABS = [
        "https://graphdb.tern.org.au/repositories/ausplots_vocabs_core/statements",
        "https://graphdb.tern.org.au/repositories/tern_vocabs_core/statements",
    ]

    ONTOLOGIES = [
        "https://raw.githubusercontent.com/ternaustralia/ontology_tern/master/docs/tern.ttl",
        "https://w3id.org/tern/ontologies/loc.ttl",
    ]

    RELOAD_MAPPINGS = True
    MAPPING_CSV = "https://docs.google.com/spreadsheets/d/1Zon-3em4OLUoPwCm4KAK2I_VRmDsu8zt4Fm6zYsNGKc/export?format=csv&gid=1421850005"
    MAPPING_YB_CSV = "https://docs.google.com/spreadsheets/d/1B2HWSTNVLQoQrNp2WM524eIJYI3rFoIfDX-M38a5L-M/export?format=csv&gid=0"

    # ---------- SHACL Validation --------------------------------------------------------------------------------------

    # SHACL Shape file path
    SHACL_SHAPE = "./shacl/plot-ontology-shacl.ttl"

    # --------- DEBUG SETTINGS -----------------------------------------------------------------------------------------

    # Set to true to execute only in one core (even if the spark job has started with more cores)
    run_on_single_process = False

    # Setting this variable to True will filter the source dataset with the indicated IDs
    DEBUG = False

    # IDs to achieve a complete Graph for 3 different Site Visits
    # DEBUG_SITE_LOCATION_VISIT_ID = [53708, 58879, 58846, 58856, 58876, 58854]
    DEBUG_SITE_LOCATION_VISIT_ID = [58880]

    DEBUG_SITE_LOCATION_VISIT_ID_STR = ["53708"]
    # DEBUG_SITE_LOCATION_VISIT_ID = [58893,58880,58994,58894,58892,57008,58891,58901,58895,58896,58623,58882,58881,58877,58963,58888]#53505, 58053, 57095, 58495, 53471, 58629, 53616, 58861]
    # DEBUG_SITE_LOCATION_VISIT_ID_STR = ["53505", "58053", "57095", "58495", "53471", "58629", "53616", "58861"]
    # DEBUG_SITE_LOCATION_VISIT_ID = [59857, 59858, 59859]
    # DEBUG_SITE_LOCATION_VISIT_ID = [58685, 53471, 58053, 58629]
    # DEBUG_SITE_LOCATION_VISIT_ID = [53543, 57633, 56922, 57637, 58692, 58746]
    # DEBUG_SITE_LOCATION_ID = [60033, 61341, 61142, 59924, 59890, 60341, 61306]
    DEBUG_SITE_LOCATION_ID = [61488]
    DEBUG_SITE_LOCATION_ID_STR = ["60033", "61341", "61142", "59924", "59890", "60341", "61306"]
    # DEBUG_SITE_LOCATION_ID = [61341, 61345, 60270, 60271, 60272]
