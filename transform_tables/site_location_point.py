import itertools
import urllib
import xml.etree.ElementTree as ET

import psycopg2 as pg
from psycopg2._psycopg import AsIs
from pyspark import Accumulator, Broadcast
from pyspark.sql import SparkSession
from pyspark.sql.types import IntegerType, StringType, StructField, StructType
from rdflib import XSD, Literal, Namespace, URIRef
from shapely.geometry import Polygon as ShapelyPolygon
from spark_etl_utils import Transform, add_error
from spark_etl_utils.coordinates.species_conservation_status import (
    generalise_coordinates,
    state_from_ausplots_id,
)
from spark_etl_utils.database import get_db_query_pg
from spark_etl_utils.rdf import Site
from spark_etl_utils.rdf.models import Point, Polygon, generate_underscore_uri
from tern_rdf import TernRdf
from tern_rdf.namespace_bindings import GEOSPARQL

from config import Config
from transform_tables.common import post_transform

SW_POINT_TYPE = "http://linked.data.gov.au/def/tern-cv/4dd22567-d692-44e7-b29d-b80eaff829dc"
CENTROID_POINT_TYPE = "http://linked.data.gov.au/def/tern-cv/7e2e87b6-c9ec-43ac-92b0-1976fc623c0c"

TERN_SITE_LOCATION_TYPE_TABLE_CREATE = """
CREATE TABLE IF NOT EXISTS ausplots.tern_site_location_point (
	site_id varchar NOT NULL,
	latitude float8 NOT NULL,
	longitude float8 NOT NULL,
	point_type varchar NOT NULL
);
"""
TERN_SITE_LOCATION_TYPE_TABLE_DELETE = """
DELETE FROM ausplots.tern_site_location_point
"""


class Table(Transform):
    def __init__(self, spark: SparkSession) -> None:
        super().__init__()
        self.spark = spark
        self.table = "site_location_point"
        self.dataset = Config.DATASET
        self.namespace = Config.DATASET_NAMESPACE

        query = """
          ( select 
                slp.*,
                sl.site_location_name as site_id,
                slp.id as unique_id,
                sl.site_location_id as site_attr_unique_id
            from ausplots.site_location_point slp
            join ausplots.site_location sl
                on slp.site_location_id = sl.site_location_id 
            where slp.longitude <> 'NaN' and slp.longitude < 155 and slp.latitude <> 'NaN' and sl.site_location_id in (
                select sl.site_location_id 
                from ausplots.site_location sl  
                join ausplots.site_location_visit slv 
                on sl.site_location_id = slv.site_location_id
            )
          ) q
        """

        self.df = get_db_query_pg(
            self.spark,
            self.db_host,
            self.db_port,
            self.db_name,
            query,
            self.db_username,
            self.db_password,
        )

        # self.df.show()

        conn = pg.connect(
            host=self.db_host,
            port=self.db_port,
            dbname=self.db_name,
            user=self.db_username,
            password=self.db_password,
        )
        src_cursor = conn.cursor()
        src_cursor.execute(TERN_SITE_LOCATION_TYPE_TABLE_CREATE)
        src_cursor.execute(TERN_SITE_LOCATION_TYPE_TABLE_DELETE)
        src_cursor.execute(
            """INSERT INTO ausplots.tern_site_location_point(site_id, latitude, longitude, point_type) VALUES ('NTAPCK2009', -12.62375, 132.860818, 'http://linked.data.gov.au/def/tern-cv/4dd22567-d692-44e7-b29d-b80eaff829dc')"""
        )
        conn.commit()
        conn.close()

        # Debug purposes
        if Config.DEBUG:
            self.df = self.df.filter(
                self.df["site_location_id"].isin(Config.DEBUG_SITE_LOCATION_ID)
            )
            # self.df.show()

    def load_lookup(self):
        ufoi_db = {
            "host": self.db_host,
            "port": self.db_port,
            "dbname": "ultimate_feature_of_interest",
            "user": self.db_username,
            "password": self.db_password,
        }
        # ecoplatform_db = {
        #     "host": self.db_host,
        #     "port": self.db_port,
        #     "dbname": self.db_name,
        #     "user": self.db_username,
        #     "password": self.db_password,
        # }

        return self.spark.sparkContext.broadcast([ufoi_db])

    def pre_transform(self, errors, warnings) -> None:
        debug_q = (
            f"""and sl.site_location_id in ({",".join(Config.DEBUG_SITE_LOCATION_ID_STR)})"""
            if Config.DEBUG
            else ""
        )
        query = f"""
            (                
                select distinct * from (
                    select distinct
                        substring(sl.site_location_name, 1,2) as state,
                        sl.site_location_id, 
                        hd.herbarium_determination as species_name
                    from ausplots.basal_area ba 
                        join ausplots.site_location_visit slv on ba.site_location_visit_id = slv.site_location_visit_id 
                        join ausplots.site_location sl on slv.site_location_id = sl.site_location_id 
                        join ausplots.herbarium_determination hd on ba.veg_barcode = hd.veg_barcode
                        join ausplots.veg_vouchers vv on ba.veg_barcode = vv.veg_barcode
                    where ba.basal_area >= 0 and ba.basal_area_factor >= 0 and slv.ok_to_publish {debug_q}
                    union
                    select distinct
                        substring(sl.site_location_name, 1,2) as state,
                        sl.site_location_id,
                        hd.herbarium_determination as species_name
                    from ausplots.veg_vouchers vv 
                        join ausplots.site_location_visit slv on vv.site_location_visit_id = slv.site_location_visit_id 
                        join ausplots.site_location sl on slv.site_location_id = sl.site_location_id
                        join ausplots.herbarium_determination hd on vv.veg_barcode = hd.veg_barcode
                    where slv.ok_to_publish {debug_q}
                    union
                    select distinct             
                        substring(sl.site_location_name, 1,2) as state,
                        sl.site_location_id,         
                        hd.herbarium_determination as species_name
                    from ausplots.point_intercept pi 
                        join ausplots.site_location_visit slv on pi.site_location_visit_id = slv.site_location_visit_id 
                        join ausplots.site_location sl on slv.site_location_id = sl.site_location_id 
                        join ausplots.herbarium_determination hd on pi.veg_barcode = hd.veg_barcode
                        join ausplots.veg_vouchers vv on pi.veg_barcode = vv.veg_barcode
                    where slv.ok_to_publish {debug_q}
                        and (
                        (lower(hd.herbarium_determination) not like '%fung%'
                        and lower(hd.herbarium_determination) not like '%lichen%'
                        and lower(vv.field_name) not like '%fung%'
                        and lower(vv.field_name) not like '%lichen%'
                        and lower(pi.growth_form) not like 'fung%'
                        and lower(vv.default_growth_form) not like 'fung%') 
                        or hd.herbarium_determination = 'Polytrichum juniperinum')
                ) q2 where q2.species_name <> 'Dead Tree/Shrub' and q2.species_name <> 'No ID' and q2.species_name <> 'Annual Forb' and q2.species_name <> 'Annual Grass'
            ) q
        """
        # print(query)
        df_species = get_db_query_pg(
            self.spark,
            self.db_host,
            self.db_port,
            self.db_name,
            query,
            self.db_username,
            self.db_password,
        )

        response = urllib.request.urlopen(Config.GENERALISATION_REPORT).read()
        tree = ET.fromstring(response)
        site_gen = []
        for row in df_species.rdd.collect():
            spp_name = row["species_name"].replace("'", "")
            species = tree.findall(f"sensitiveSpecies[@name='{spp_name}']")
            for s in species:
                instances = s.find("instances").findall("conservationInstance")
                for i in instances:
                    state = state_from_ausplots_id(row["state"])
                    if i.attrib["zone"] == state:
                        # Site must be generalised
                        if row["site_location_id"] not in site_gen:
                            site_gen.append((row["site_location_id"], i.attrib["generalisation"]))

        mySchema = StructType(
            [
                StructField("site_location_id_aux", IntegerType()),
                StructField("generalisation", StringType()),
            ]
        )
        df_site_gen = self.spark.createDataFrame(data=site_gen, schema=mySchema)
        self.df = self.df.join(
            df_site_gen, self.df.site_location_id == df_site_gen.site_location_id_aux, "left"
        )
        # self.df.show()

    @staticmethod
    def transform(
        rows: itertools.chain,
        dataset: str,
        namespace_url: str,
        table_name: str,
        vocabulary_mappings: Broadcast,
        vocabulary_graph: Broadcast,
        errors: Accumulator,
        warnings: Accumulator,
        lookup: Broadcast = None,
        ontology: Broadcast = None,
    ) -> None:
        ns = Namespace(namespace_url)

        LATITUDE = "latitude"
        LONGITUDE = "longitude"

        site_points_list = list()

        ufoi_db = lookup.value[0]
        conn = pg.connect(
            host=ufoi_db["host"],
            port=ufoi_db["port"],
            dbname=ufoi_db["dbname"],
            user=ufoi_db["user"],
            password=ufoi_db["password"],
        )

        g = TernRdf.Graph([dataset.upper()])

        grouped_rows_by_site = dict()
        for row in rows:
            """
            Example result:
            {
                "SAAEYB0001": {
                    "CENTRE": Row(
                        id=1367071,
                        site_location_id=60372,
                        point='CENTRE',
                        easting=717265.818,
                        northing=6184704.08,
                        latitude=-34.456358333333334,
                        longitude=137.36516944444443,
                        zone=53,
                        threedcq=6.038,
                        site_visit_id=57637,
                        site_id='SAAEYB0001',
                        default_datetime=datetime.datetime(2015, 10, 12, 15, 0, 12),
                        unique_id=1367071
                    ),
                    "N1": {
                        ...
                    },
                    "N2": {
                        ...
                    }
                },
                "WAANOK0005": {
                    ...
                }
            }
            """
            k = row["site_id"]
            site_points = grouped_rows_by_site.get(k, dict())
            site_points.update({row["point"]: row})
            grouped_rows_by_site.update({k: site_points})

        for site_grouped in grouped_rows_by_site.items():
            site_id = site_grouped[0]
            site_uri = Site.generate_uri(ns, site_id)
            point_dict = site_grouped[1]
            if "SW" not in point_dict:
                add_error(
                    warnings,
                    "Did not find SW Point in site_location_point table for SITE_LOCATION_ID={}, "
                    "calculating centroid".format(site_id),
                )

                # Calculate centroid
                E_latitudes = 0
                E_longitudes = 0
                # generalisation = None
                for p in point_dict.values():
                    # generalisation = p["generalisation"]
                    E_latitudes += p[LATITUDE]
                    E_longitudes += p[LONGITUDE]

                lat = E_latitudes / len(point_dict)
                long = E_longitudes / len(point_dict)

                # lat, long, diff_lat, diff_long = generalise_coordinates(generalisation, lat, long)

                point_uri = URIRef(generate_underscore_uri(ns))
                g.add((site_uri, GEOSPARQL.hasGeometry, point_uri))
                point = Point(
                    uri=point_uri,
                    as_wkt=Literal(
                        "POINT({} {})".format(round(long, 8), round(lat, 8)),
                        datatype=GEOSPARQL.wktLiteral,
                    ),
                    latitude=Literal(lat, datatype=XSD.double),
                    longitude=Literal(long, datatype=XSD.double),
                    point_type=URIRef(CENTROID_POINT_TYPE),
                )
                g += point.g

                # add point to dictionary to store in DB
                site_points_list.append(
                    {
                        "site_id": site_id,
                        "latitude": lat,
                        "longitude": long,
                        "point_type": SW_POINT_TYPE,
                    }
                )
            else:
                location_SW = point_dict.get("SW")
                lat = location_SW[LATITUDE]
                long = location_SW[LONGITUDE]
                diff_lat = 0
                diff_long = 0
                # lat, long, diff_lat, diff_long = generalise_coordinates(
                #     location_SW["generalisation"], lat, long
                # )

                point_uri = URIRef(generate_underscore_uri(ns))
                g.add((site_uri, GEOSPARQL.hasGeometry, point_uri))
                point = Point(
                    uri=point_uri,
                    as_wkt=Literal(
                        "POINT({} {})".format(round(long, 8), round(lat, 8)),
                        datatype=GEOSPARQL.wktLiteral,
                    ),
                    latitude=Literal(lat, datatype=XSD.double),
                    longitude=Literal(long, datatype=XSD.double),
                    point_type=URIRef(SW_POINT_TYPE),
                )
                g += point.g

                # add point to dictionary to store in DB
                site_points_list.append(
                    {
                        "site_id": site_id,
                        "latitude": lat,
                        "longitude": long,
                        "point_type": SW_POINT_TYPE,
                    }
                )

                if "NW" not in point_dict:
                    add_error(
                        warnings,
                        "Did not find NW Point in site_location_point table for "
                        "SITE_LOCATION_ID={}".format(site_grouped[0]),
                    )
                elif "NE" not in point_dict:
                    add_error(
                        warnings,
                        "Did not find NE Point in site_location_point table for "
                        "SITE_LOCATION_ID={}".format(site_grouped[0]),
                    )
                elif "SE" not in point_dict:
                    add_error(
                        warnings,
                        "Did not find SE Point in site_location_point table for "
                        "SITE_LOCATION_ID={}".format(site_grouped[0]),
                    )
                else:
                    polygon = "POLYGON(("
                    point_SW = point_dict.get("SW")
                    polygon += (
                        str(round(point_SW[LONGITUDE] + diff_long, 8))
                        + " "
                        + str(round(point_SW[LATITUDE] + diff_lat, 8))
                        + ", "
                    )
                    point_NW = point_dict.get("NW")
                    polygon += (
                        str(round(point_NW[LONGITUDE] + diff_long, 8))
                        + " "
                        + str(round(point_NW[LATITUDE] + diff_lat, 8))
                        + ", "
                    )
                    point_NE = point_dict.get("NE")
                    polygon += (
                        str(round(point_NE[LONGITUDE] + diff_long, 8))
                        + " "
                        + str(round(point_NE[LATITUDE] + diff_lat, 8))
                        + ", "
                    )
                    point_SE = point_dict.get("SE")
                    polygon += (
                        str(round(point_SE[LONGITUDE] + diff_long, 8))
                        + " "
                        + str(round(point_SE[LATITUDE] + diff_lat, 8))
                        + ", "
                    )
                    polygon += (
                        str(round(point_SW[LONGITUDE] + diff_long, 8))
                        + " "
                        + str(round(point_SW[LATITUDE] + diff_lat, 8))
                    )
                    polygon += "))"
                    polygon_shapely = [
                        (point_SW[LONGITUDE] + diff_long, point_SW[LATITUDE] + diff_lat),
                        (point_NW[LONGITUDE] + diff_long, point_NW[LATITUDE] + diff_lat),
                        (point_NE[LONGITUDE] + diff_long, point_NE[LATITUDE] + diff_lat),
                        (point_SE[LONGITUDE] + diff_long, point_SE[LATITUDE] + diff_lat),
                        (point_SW[LONGITUDE] + diff_long, point_SW[LATITUDE] + diff_lat),
                    ]

                    if "nan" not in polygon.lower():
                        if ShapelyPolygon(polygon_shapely).is_valid:
                            polygon_uri = URIRef(generate_underscore_uri(ns))
                            g.add((site_uri, GEOSPARQL.hasGeometry, polygon_uri))
                            polygon = Polygon(
                                polygon_uri, as_wkt=Literal(polygon, datatype=GEOSPARQL.wktLiteral)
                            )
                            g += polygon.g
                        else:
                            add_error(
                                warnings,
                                "Invalid site-polygon in site_location_point table for "
                                "SITE_LOCATION_ID={}".format(site_grouped[0]),
                            )
                    else:
                        add_error(
                            warnings,
                            "Some point has NaN value in site_location_point table for "
                            "SITE_LOCATION_ID={}".format(site_grouped[0]),
                        )

            # src_cursor = conn.cursor("serverCursor")
            # src_cursor.execute(
            #     "select uri, supersite_name, supersite_type from supersites where ST_Intersects(geom, ST_SetSRID(ST_MakePoint(%(lon)s, %(lat)s), 3577))",
            #     {'lon': long, 'lat': lat})
            # result = src_cursor.fetchone()
            # if result is not None:
            #     g.add((site_uri, DCTERMS.isPartOf, URIRef(result[0])))
            #     g.add((URIRef(result[0]), DCTERMS.identifier, Literal(result[1])))
            #     g.add((URIRef(result[0]), RDF.type, URIRef(result[2])))
            # src_cursor.close()

            # Calculate regions from TERN PostGIS
            i = 1
            for ufoi_key in Config.ufoi_list.keys():
                src_cursor = conn.cursor("serverCursor")
                src_cursor.execute(
                    "select %(value)s from %(table_name)s where ST_Intersects(geom, ST_SetSRID(ST_MakePoint(%(lon)s, %(lat)s), 3577))",
                    {
                        "table_name": AsIs(ufoi_key),
                        "value": AsIs(Config.ufoi_list[ufoi_key][1]),
                        "lon": long,
                        "lat": lat,
                    },
                )
                result = src_cursor.fetchone()
                if result is not None:
                    region_parent = Config.ufoi_list[ufoi_key][0]
                    region_item = result[0]
                    g.add(
                        (
                            site_uri,
                            GEOSPARQL.sfWithin,
                            URIRef("{}{}".format(region_parent, region_item)),
                        )
                    )
                    i += 1
                src_cursor.close()

        site_uri = Site.generate_uri(ns, "NTAPCK2009")
        point_uri = URIRef(generate_underscore_uri(ns))
        lat = -12.62375
        long = 132.860818
        g.add((site_uri, GEOSPARQL.hasGeometry, point_uri))
        point = Point(
            uri=point_uri,
            as_wkt=Literal(
                "POINT({} {})".format(round(long, 8), round(lat, 8)),
                datatype=GEOSPARQL.wktLiteral,
            ),
            latitude=Literal(lat, datatype=XSD.double),
            longitude=Literal(long, datatype=XSD.double),
            point_type=URIRef(SW_POINT_TYPE),
        )
        g += point.g

        # add point to dictionary to store in DB
        # site_points_list.append(
        #     {
        #         "site_id": site_id,
        #         "latitude": lat,
        #         "longitude": long,
        #         "point_type": SW_POINT_TYPE,
        #     }
        # )

        i = 1
        for ufoi_key in Config.ufoi_list.keys():
            src_cursor = conn.cursor("serverCursor")
            src_cursor.execute(
                "select %(value)s from %(table_name)s where ST_Intersects(geom, ST_SetSRID(ST_MakePoint(%(lon)s, %(lat)s), 3577))",
                {
                    "table_name": AsIs(ufoi_key),
                    "value": AsIs(Config.ufoi_list[ufoi_key][1]),
                    "lon": long,
                    "lat": lat,
                },
            )
            result = src_cursor.fetchone()
            if result is not None:
                region_parent = Config.ufoi_list[ufoi_key][0]
                region_item = result[0]
                g.add(
                    (
                        site_uri,
                        GEOSPARQL.sfWithin,
                        URIRef("{}{}".format(region_parent, region_item)),
                    )
                )
                i += 1
            src_cursor.close()
        conn.close()

        conn = pg.connect(
            host=ufoi_db["host"],
            port=ufoi_db["port"],
            dbname="ecoplatform",
            user=ufoi_db["user"],
            password=ufoi_db["password"],
        )
        src_cursor = conn.cursor()
        src_cursor.executemany(
            """INSERT INTO ausplots.tern_site_location_point(site_id, latitude, longitude, point_type) VALUES (%(site_id)s, %(latitude)s, %(longitude)s, %(point_type)s)""",
            site_points_list,
        )

        conn.commit()
        conn.close()

        post_transform(g, ontology.value, table_name)
