import itertools

from pyspark import Broadcast, Accumulator
from pyspark.sql import SparkSession
from pyspark.sql.functions import udf
from pyspark.sql.types import (
    FloatType,
    StructField,
    StringType,
    BooleanType,
    DoubleType,
    TimestampType,
    IntegerType,
    StructType,
)
from rdflib import Namespace, Literal, URIRef, XSD
from spark_etl_utils import Transform, add_error
from spark_etl_utils.database import (
    get_db_query_pg,
    save_dataframe_as_jdbc_table,
    get_postgres_url,
    get_db_table_pandas,
)
from spark_etl_utils.rdf import (
    generate_rdf_graph,
    SITE_LOCATION_ID,
    SITE_LOCATION_VISIT_ID,
    SITE_TYPE_SITE,
)
from spark_etl_utils.rdf.models import Dataset, SiteVisit, Site

from config import Config
from transform_tables.common import post_transform

TERN_SITE_SCHEMA = StructType(
    [
        StructField("site_location_id", IntegerType(), True),
        StructField("site_location_name", StringType(), True),
        StructField("established_date", TimestampType(), True),
        StructField("description", StringType(), True),
        StructField("bioregion_name", StringType(), True),
        StructField("property", StringType(), True),
        StructField("paddock", StringType(), True),
        StructField("plot_is_permanently_marked", BooleanType(), True),
        StructField("plot_is_aligned_to_grid", BooleanType(), True),
        StructField("plot_is_100m_by_100m", BooleanType(), True),
        StructField("landform_pattern", StringType(), True),
        StructField("landform_element", StringType(), True),
        StructField("site_slope", StringType(), True),
        StructField("site_aspect", StringType(), True),
        StructField("plot_dimensions", StringType(), True),
        StructField("comments", StringType(), True),
        StructField("app_lat", StringType(), True),
        StructField("app_long", StringType(), True),
        StructField("outcrop_lithology", StringType(), True),
        StructField("other_outcrop_lithology", StringType(), True),
        StructField("surface_strew_size", IntegerType(), True),
        StructField("surface_strew_lithology", StringType(), True),
        StructField("site_visit_id", IntegerType(), True),
        StructField("site_id", StringType(), True),
        StructField("landform_id", StringType(), True),
        StructField("land_surface_id", StringType(), True),
        StructField("default_datetime", TimestampType(), True),
        StructField("unique_id", IntegerType(), True),
        StructField("site_attr_unique_id", IntegerType(), True),
        StructField("latitude", FloatType(), True),
        StructField("longitude", FloatType(), True),
        StructField("plot_width", DoubleType(), True),
        StructField("plot_length", DoubleType(), True),
        StructField("plot_area", DoubleType(), True),
        StructField("plot_shape", StringType(), True),
    ]
)

PLOT_SHAPE_SQUARE = "http://linked.data.gov.au/def/tern-cv/b2e07a7f-942e-44d9-9fd6-f0cee7be103d"
PLOT_SHAPE_RECTANGLE = "http://linked.data.gov.au/def/tern-cv/c4215f3c-1f25-4c1c-b910-27027848fc8f"


def get_plot_dimensions(dimensions):
    if dimensions in (
        "100 x 100m.",
        "'100  x  100'",
        "100m x 100m.",
        "100 X 100m.",
        "100 x 100m",
        "100 x 100",
        "100m. x 100m.",
        "100  x  100 m",
        "100 x 100 m.",
    ):
        return 100.0, 100.0, 100.0 * 100.0, PLOT_SHAPE_SQUARE
    elif dimensions in ("'200  x  50m.'", "200 m. x 50 m.", "200  x  50 m.", "'200  x  50 m.'"):
        return 50.0, 200.0, 50.0 * 200.0, PLOT_SHAPE_RECTANGLE
    elif dimensions == "100 x 1600m.":
        return 100.0, 1600.0, 100.0 * 1600.0, PLOT_SHAPE_RECTANGLE
    else:
        return 100.0, 100.0, 100.0 * 100.0, PLOT_SHAPE_SQUARE


class Table(Transform):
    def __init__(self, spark: SparkSession) -> None:
        super().__init__()
        self.spark = spark
        self.table = "site_location"
        self.dataset = Config.DATASET
        self.namespace = Config.DATASET_NAMESPACE

        query = """
          (select 
                sl.*,
                slv.site_location_visit_id as site_visit_id, 
                sl.site_location_name as site_id,
                sl.site_location_name as landform_id,
                sl.site_location_name as land_surface_id,
                slv.visit_start_date as default_datetime,  
                slv.site_location_visit_id as unique_id,
                sl.site_location_id as site_attr_unique_id
            from ausplots.site_location sl 
                join ausplots.site_location_visit slv on sl.site_location_id = slv.site_location_id
            where sl.site_location_id not in (61427,61325,61446,61447,61431,61425,61439,61440)
          ) q
        """

        self.df = get_db_query_pg(
            self.spark,
            self.db_host,
            self.db_port,
            self.db_name,
            query,
            self.db_username,
            self.db_password,
        )

        # self.df.show()

        # Debug purposes
        if Config.DEBUG:
            self.df = self.df.filter(
                self.df["site_location_id"].isin(Config.DEBUG_SITE_LOCATION_ID)
            )
            # self.df.show()

    def pre_transform(self, errors, warnings) -> None:
        self.table = "tern_{}".format(self.table)
        get_plot_dimensions_width_udf = udf(lambda z: get_plot_dimensions(z)[0], FloatType())
        get_plot_dimensions_length_udf = udf(lambda z: get_plot_dimensions(z)[1], FloatType())
        get_plot_dimensions_area_udf = udf(lambda z: get_plot_dimensions(z)[2], FloatType())
        get_plot_dimensions_shape_udf = udf(lambda z: get_plot_dimensions(z)[3], StringType())
        df_derived = self.df.withColumn(
            "plot_width", get_plot_dimensions_width_udf("plot_dimensions")
        )
        df_derived = df_derived.withColumn(
            "plot_length", get_plot_dimensions_length_udf("plot_dimensions")
        )
        df_derived = df_derived.withColumn(
            "plot_area", get_plot_dimensions_area_udf("plot_dimensions")
        )
        df_derived = df_derived.withColumn(
            "plot_shape", get_plot_dimensions_shape_udf("plot_dimensions")
        )

        self.df = df_derived
        save_dataframe_as_jdbc_table(
            self.df,
            get_postgres_url(
                self.db_host, self.db_port, self.db_name, self.db_username, self.db_password
            ),
            self.dataset,
            self.table,
        )

    def load_lookup(self):
        dataset_version = get_db_table_pandas(
            self.db_host,
            self.db_port,
            self.db_name,
            "r_ecoplatform_metadata",
            self.db_username,
            self.db_password,
            Config.DATASET,
            query="select load_date from {}.r_ecoplatform_metadata".format(Config.DATASET),
        ).iloc[0, 0]

        return self.spark.sparkContext.broadcast([dataset_version])

    @staticmethod
    def transform(
        rows: itertools.chain,
        dataset: str,
        namespace_url: str,
        table_name: str,
        vocabulary_mappings: Broadcast,
        vocabulary_graph: Broadcast,
        errors: Accumulator,
        lookup: Broadcast = None,
        ontology: Broadcast = None,
    ) -> None:
        # In order to loop over the "rows" iterator 2 times, we need to create a copy (as the iterator
        # is exhausted after the first loop).
        rows, rows_cloned = itertools.tee(rows)
        g = generate_rdf_graph(
            rows,
            dataset,
            Namespace(namespace_url),
            vocabulary_mappings,
            vocabulary_graph,
            errors,
        )
        dataset_uri = Dataset.generate_uri(Namespace(namespace_url))
        g += Dataset(
            uri=dataset_uri,
            title=Literal("TERN Surveillance Monitoring"),
            description=Literal("TERN Surveillance Monitoring"),
            issued=Literal(lookup.value[0], datatype=XSD.date),
            citation=Literal("TERN Ecosystem Surveillance Program."),
            creator=Literal("TERN Ecosystem Surveillance Program"),
            publisher=Literal("TERN Ecosystem Surveillance Program"),
        ).g
        for row in rows_cloned:
            site_uri = Site.generate_uri(Namespace(namespace_url), row[SITE_LOCATION_ID])
            site_visit_uri = SiteVisit.generate_uri(
                Namespace(namespace_url), row[SITE_LOCATION_VISIT_ID]
            )
            try:
                established_date = row["established_date"].strftime("%Y-%m-%d")
                g += Site(
                    uri=site_uri,
                    identifier=Literal(row[SITE_LOCATION_ID], datatype=XSD.string),
                    in_dataset=dataset_uri,
                    date_commissioned=Literal(established_date, datatype=XSD.date),
                    has_site_visit=site_visit_uri,
                    location_description=Literal(row["description"], datatype=XSD.string),
                    # location_procedure=,
                    # polygon=, # Table site_location_point gives this value
                    # site_description=Literal(row["description"]),
                    feature_type=URIRef(SITE_TYPE_SITE),
                    # date_decommissioned=,
                    # has_part=,
                    # is_part_of=,
                    dimension=Literal(
                        "{}x{}".format(row["plot_width"], row["plot_length"]), datatype=XSD.string
                    ),
                    # has_geometry=, # Table site_location_point gives this value
                    # sf_within=, # Table site_location_point gives this value
                ).g
            except Exception as e:
                print(e)
                add_error(
                    errors,
                    "Site establishment date missing or has wrong format for SITE_LOCATION_ID={}.".format(
                        row[SITE_LOCATION_ID]
                    ),
                )

        post_transform(g, ontology.value, table_name)
