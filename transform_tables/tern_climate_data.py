import itertools
import math

import psycopg2 as pg
from psycopg2._psycopg import AsIs
from pyspark import Accumulator, Broadcast
from pyspark.sql import SparkSession
from pyspark.sql.types import (
    Row,
    StructField,
    StructType,
    StringType,
    TimestampType,
    LongType,
    DoubleType,
)
from rdflib import Namespace
from spark_etl_utils import Transform, add_error
from spark_etl_utils.database import get_db_query_pg, save_dataframe_as_jdbc_table, get_postgres_url
from spark_etl_utils.rdf import generate_rdf_graph

from config import Config
from transform_tables.common import post_transform

DB_SCHEMA_TABLE = "climate_data.climate_cache_postgis"
TERN_CLIMATE_SCHEMA = StructType(
    [
        StructField("site_visit_id", LongType(), True),
        StructField("site_id", StringType(), True),
        StructField("default_datetime", TimestampType(), True),
        StructField("weather_id", StringType(), True),
        StructField("unique_id", StringType(), True),
        StructField("latitude", DoubleType(), True),
        StructField("longitude", DoubleType(), True),
        StructField("precipitation_annual_mean", DoubleType(), True),
        StructField("temperature_annual_mean", DoubleType(), True),
        StructField("temperature_max", DoubleType(), True),
        StructField("temperature_min", DoubleType(), True),
        StructField("solar_radiation", DoubleType(), True),
    ]
)


def clean_value(value):
    if value == -9999 or value == -999 or math.isnan(value):
        return None
    else:
        return value


class Table(Transform):
    def __init__(self, spark: SparkSession) -> None:
        super().__init__()
        self.spark = spark
        self.table = "tern_climate_data"
        self.dataset = Config.DATASET
        self.namespace = Config.DATASET_NAMESPACE

        query = """
          ( select 	 
                slv.site_location_visit_id as site_visit_id,   
                sl.site_location_name as site_id,
                slv.visit_start_date as default_datetime,  
                sl.site_location_name as weather_id,
                slv.site_location_visit_id as unique_id,
                slp.latitude,
                slp.longitude 
            from ausplots.site_location_point slp 
                join ausplots.site_location_visit slv on slp.site_location_id=slv.site_location_id 
                join ausplots.site_location sl on slv.site_location_id = sl.site_location_id 
            where slp.point='SW'
          ) q
        """

        self.df = get_db_query_pg(
            self.spark,
            self.db_host,
            self.db_port,
            self.db_name,
            query,
            self.db_username,
            self.db_password,
        )

        # self.df.show()

        # Debug purposes
        if Config.DEBUG:
            self.df = self.df.filter(
                self.df["site_visit_id"].isin(Config.DEBUG_SITE_LOCATION_VISIT_ID)
            )

    def pre_transform(self, errors, warnings):
        conn = pg.connect(
            host=self.db_host,
            port=self.db_port,
            dbname=self.db_name,
            user=self.db_username,
            password=self.db_password,
        )
        new_rows = []
        for row in self.df.rdd.collect():
            lat = row["latitude"]
            lon = row["longitude"]
            cursor = conn.cursor()
            cursor.execute(
                "SELECT * FROM %(table_name)s WHERE ST_Intersects(geom, ST_SetSRID(ST_MakePoint(%(lon)s, %(lat)s), 3577))",
                {"table_name": AsIs(DB_SCHEMA_TABLE), "lon": lon, "lat": lat},
            )
            result = cursor.fetchone()
            cursor.close()
            if result:
                new_rows.append(
                    Row(
                        **row.asDict(),
                        precipitation_annual_mean=clean_value(result[2]),
                        temperature_annual_mean=clean_value(result[3]),
                        temperature_max=clean_value(result[4]),
                        temperature_min=clean_value(result[5]),
                        solar_radiation=clean_value(result[6]),
                    )
                )
                # print(new_rows)
            else:
                add_error(
                    warnings,
                    "Did not find CLIMATE DATA in {} table for coordinates lat='{}', lon='{}' of SITE_LOCATION_ID={}".format(
                        DB_SCHEMA_TABLE, lat, lon, row["site_id"]
                    ),
                )

        self.df = self.spark.createDataFrame(new_rows, TERN_CLIMATE_SCHEMA)
        save_dataframe_as_jdbc_table(
            self.df,
            get_postgres_url(
                self.db_host, self.db_port, self.db_name, self.db_username, self.db_password
            ),
            self.dataset,
            self.table,
        )

    @staticmethod
    def transform(
        rows: itertools.chain,
        dataset: str,
        namespace_url: str,
        table_name: str,
        vocabulary_mappings: Broadcast,
        vocabulary_graph: Broadcast,
        errors: Accumulator,
        warnings: Accumulator,
        lookup: Broadcast = None,
        ontology: Broadcast = None,
    ) -> None:
        g = generate_rdf_graph(
            rows,
            dataset,
            Namespace(namespace_url),
            vocabulary_mappings,
            vocabulary_graph,
            errors,
            warnings,
        )
        post_transform(g, ontology.value, table_name)
