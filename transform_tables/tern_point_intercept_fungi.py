import itertools

from pyspark import Accumulator, Broadcast
from pyspark.sql import SparkSession
from rdflib import XSD, Literal, Namespace, URIRef
from spark_etl_utils import Transform
from spark_etl_utils.database import get_db_query_pg
from spark_etl_utils.rdf import SITE_TYPE_TRANSECT, generate_rdf_graph
from spark_etl_utils.rdf.models import RDFDataset, Site, SiteVisit, Transect

from config import Config
from transform_tables.common import post_transform


class Table(Transform):
    def __init__(self, spark: SparkSession):
        super().__init__()
        self.spark = spark
        self.table = "tern_point_intercept_fungi"
        self.dataset = Config.DATASET
        self.namespace = Config.DATASET_NAMESPACE
        self.matched_species = {}

        query = """
          ( select 
                pi.*,
                pi.point_number as point_number_substrate,
                pi.site_location_visit_id as site_visit_id,
                concat(sl.site_location_name, '-transect-', pi.transect) as site_id,
                sl.site_location_name as parent_site_id,
                slv.visit_start_date as default_datetime,
                pi.id as fungal_occurr_id,  
                hd.herbarium_determination as species_name,
                hd.herbarium_determination as species_name_attr,
                vv.field_name as field_name,
                pi.id as land_surface_substrate_id,
                pi.id as unique_id
            from ausplots.point_intercept pi 
                join ausplots.site_location_visit slv on pi.site_location_visit_id = slv.site_location_visit_id 
                join ausplots.site_location sl on slv.site_location_id = sl.site_location_id 
                join ausplots.herbarium_determination hd on pi.veg_barcode = hd.veg_barcode
                join ausplots.veg_vouchers vv on pi.veg_barcode = vv.veg_barcode
            where hd.herbarium_determination not like 'Polytrichum juniperinum' 
                and (hd.herbarium_determination like '%fungi%'
                or hd.herbarium_determination like '%lichen%'
                or hd.herbarium_determination like '%fungus%'
                or vv.field_name like '%fungi%'
                or vv.field_name like '%lichen%'
                or vv.field_name like '%fungus%'
                or pi.growth_form like 'Fungus')
          ) q
        """

        self.df = get_db_query_pg(
            self.spark,
            self.db_host,
            self.db_port,
            self.db_name,
            query,
            self.db_username,
            self.db_password,
        )

        # self.df.show()

        # Debug purposes
        # if Config.DEBUG:
        #     self.df = self.df.filter(self.df["site_visit_id"].isin(Config.DEBUG_SITE_LOCATION_VISIT_ID))
        # self.df.show()

    @staticmethod
    def transform(
        rows: itertools.chain,
        dataset: str,
        namespace_url: str,
        table_name: str,
        vocabulary_mappings: Broadcast,
        vocabulary_graph: Broadcast,
        errors: Accumulator,
        warnings: Accumulator,
        lookup: Broadcast = None,
        ontology: Broadcast = None,
    ) -> None:
        ns = Namespace(namespace_url)
        # matched_species = lookup.value
        # In order to loop over the "rows" iterator 2 times, we need to create a copy (as the iterator
        # is exhausted after the first loop).
        rows, rows_cloned = itertools.tee(rows)
        g = generate_rdf_graph(
            rows,
            dataset,
            ns,
            vocabulary_mappings,
            vocabulary_graph,
            errors,
            warnings,
        )
        dataset_uri = RDFDataset.generate_uri(ns)

        for row in rows_cloned:
            # Create transects
            transect = Transect(
                uri=Transect.generate_uri(ns, row["site_id"]),
                identifier=Literal(row["site_id"], datatype=XSD.string),
                in_dataset=dataset_uri,
                is_sample_of=Site.generate_uri(ns, row["parent_site_id"]),
                feature_type=URIRef(SITE_TYPE_TRANSECT),
                has_site_visit=SiteVisit.generate_uri(ns, row["site_visit_id"]),
                transect_direction=Literal(row["transect"], datatype=XSD.string),
            )
            g += transect.g

        post_transform(g, ontology.value, table_name)
