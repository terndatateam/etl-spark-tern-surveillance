import itertools

from pyspark import Accumulator, Broadcast
from pyspark.sql import SparkSession
from pyspark.sql.functions import count
from rdflib import Namespace
from spark_etl_utils import Transform
from spark_etl_utils.database import (
    get_db_query_pg,
    get_db_table_pandas,
    get_postgres_url,
    save_dataframe_as_jdbc_table,
)
from spark_etl_utils.rdf import generate_rdf_graph

from config import Config
from pre_tranforms.fractional_cover import fractional_cover_R
from transform_tables.common import post_transform


class Table(Transform):
    def __init__(self, spark: SparkSession):
        super().__init__()
        self.spark = spark
        self.table = "tern_derived_ground_cover"
        self.dataset = Config.DATASET
        self.namespace = Config.DATASET_NAMESPACE

        query = """
          ( select
                pi.site_location_visit_id as site_visit_id,
                pi.substrate
            from ausplots.point_intercept pi 
                join ausplots.site_location_visit slv on pi.site_location_visit_id = slv.site_location_visit_id            
          ) q
        """

        self.df = get_db_query_pg(
            self.spark,
            self.db_host,
            self.db_port,
            self.db_name,
            query,
            self.db_username,
            self.db_password,
        )

        # self.df.show()

        # Debug purposes
        if Config.DEBUG:
            self.df = self.df.filter(
                self.df["site_visit_id"].isin(Config.DEBUG_SITE_LOCATION_VISIT_ID)
            )
            # self.df.show()

    def pre_transform(self, errors, warnings):
        query = """
            select pi2.*, 
                slv.site_location_id, 
                sl.site_location_name, 
                concat(sl.site_location_name,'-', slv.site_location_visit_id) as site_unique, 
                concat(pi2.transect,' ', pi2.point_number) as hits_unique
            from ausplots.point_intercept pi2 left join ausplots.site_location_visit slv on pi2.site_location_visit_id = slv.site_location_visit_id 
            join ausplots.site_location sl on slv.site_location_id = sl.site_location_id
            where slv.ok_to_publish
        """
        veg_pd_df = get_db_table_pandas(
            self.db_host,
            self.db_port,
            self.db_name,
            "site_location_visit",
            self.db_username,
            self.db_password,
            Config.DATASET,
            query=query,
        )
        fractional_cover_pd_df = fractional_cover_R(veg_pd_df)
        fractional_cover_spark_df = self.spark.createDataFrame(fractional_cover_pd_df)

        save_dataframe_as_jdbc_table(
            fractional_cover_spark_df,
            get_postgres_url(
                self.db_host, self.db_port, self.db_name, self.db_username, self.db_password
            ),
            self.dataset,
            "tern_derived_ground_cover_temp",
        )

        fractional_cover_spark_df.createOrReplaceTempView("fractional_cover_spark_df")
        df_derived = self.df.groupBy("site_visit_id").pivot("substrate").agg(count("*")).fillna(0)

        query = """
            (   select
                    slv.site_location_visit_id as site_visit_id,
                    sl.site_location_name as site_id,
                    sl.site_location_name as land_surface_substrate_id,
                    slv.visit_start_date as default_datetime
                from ausplots.site_location_visit slv
                    join ausplots.site_location sl on slv.site_location_id = sl.site_location_id
                where slv.ok_to_publish
            ) q
        """

        site_df = get_db_query_pg(
            self.spark,
            self.db_host,
            self.db_port,
            self.db_name,
            query,
            self.db_username,
            self.db_password,
        )

        df_derived = df_derived.join(site_df, "site_visit_id")
        df_derived.createOrReplaceTempView("df_derived")

        save_dataframe_as_jdbc_table(
            df_derived,
            get_postgres_url(
                self.db_host, self.db_port, self.db_name, self.db_username, self.db_password
            ),
            self.dataset,
            "tern_derived_ground_cover_temp2",
        )

        df_total_gc = self.spark.sql(
            """select 
                    t.site_id,
                    t.site_visit_id,
                    t.land_surface_substrate_id,
                    t.default_datetime,
                    fc.site_unique as unique_id, 
                    t.Bare/total_points*(100-fc.green) as bare, 
                    CWD/total_points*(100-fc.green) as cwd, 
                    Crypto/total_points*(100-fc.green) as crypto, 
                    Gravel/total_points*(100-fc.green) as gravel, 
                    Litter/total_points*(100-fc.green) as litter, 
                    NC/total_points*(100-fc.green) as nc, 
                    Outcrop/total_points*(100-fc.green) as outcrop, 
                    Rock/total_points*(100-fc.green) as rock, 
                    Unkwn/total_points*(100-fc.green) as unkwn, 
                    fc.green as green
                from 
                    (select *, Bare + CWD + Crypto + Gravel + Litter + NC + Outcrop + Rock + Unkwn as total_points 
                    from df_derived) t join fractional_cover_spark_df as fc 
                on t.site_visit_id=fc.site_location_visit_id"""
        )

        save_dataframe_as_jdbc_table(
            df_total_gc,
            get_postgres_url(
                self.db_host, self.db_port, self.db_name, self.db_username, self.db_password
            ),
            self.dataset,
            "tern_derived_ground_cover",
        )

        self.df = df_total_gc

    @staticmethod
    def transform(
        rows: itertools.chain,
        dataset: str,
        namespace_url: str,
        table_name: str,
        vocabulary_mappings: Broadcast,
        vocabulary_graph: Broadcast,
        errors: Accumulator,
        warnings: Accumulator,
        lookup: Broadcast = None,
        ontology: Broadcast = None,
    ) -> None:
        g = generate_rdf_graph(
            rows,
            dataset,
            Namespace(namespace_url),
            vocabulary_mappings,
            vocabulary_graph,
            errors,
            warnings,
        )
        post_transform(g, ontology.value, table_name)
