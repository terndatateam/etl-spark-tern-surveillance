import itertools

from pyspark import Accumulator, Broadcast
from pyspark.sql import SparkSession
from pyspark.sql.functions import udf
from pyspark.sql.types import StringType
from rdflib import Namespace
from spark_etl_utils import Transform, clean_string
from spark_etl_utils.database import get_db_query_pg
from spark_etl_utils.rdf import generate_rdf_graph

from config import Config
from transform_tables.common import post_transform


class Table(Transform):
    def __init__(self, spark: SparkSession) -> None:
        super().__init__()
        self.spark = spark
        self.table = "characterisation_coarse_frags_abundance"
        self.dataset = Config.DATASET
        self.namespace = Config.DATASET_NAMESPACE

        query = """
          ( select 
                ccfa.*,
                sc.site_location_visit_id as site_visit_id, 
                sl.site_location_name as site_id,
                slv.visit_start_date as default_datetime,
                ccfa.soil_characterisation_id as soil_char_id,
                sc.layer_barcode as sample_id,
                ccfa.id as unique_id
            from ausplots.characterisation_coarse_frags_abundance ccfa 
            join ausplots.soil_characterisation sc on ccfa.soil_characterisation_id = sc.id
                join ausplots.site_location_visit slv on sc.site_location_visit_id = slv.site_location_visit_id 
                join ausplots.site_location sl on slv.site_location_id = sl.site_location_id 
            where ccfa.coarse_frag_abund_id != 'NC'
          ) q
        """

        self.df = get_db_query_pg(
            self.spark,
            self.db_host,
            self.db_port,
            self.db_name,
            query,
            self.db_username,
            self.db_password,
        )

        # self.df.show()

        # Debug purposes
        if Config.DEBUG:
            self.df = self.df.filter(
                self.df["site_visit_id"].isin(Config.DEBUG_SITE_LOCATION_VISIT_ID)
            )
            # self.df.show()

    def pre_transform(self, errors, warnings):
        decode_udf = udf(lambda val: clean_string(val), StringType())
        self.df = self.df.withColumn("sample_id", decode_udf("sample_id"))

    @staticmethod
    def transform(
        rows: itertools.chain,
        dataset: str,
        namespace_url: str,
        table_name: str,
        vocabulary_mappings: Broadcast,
        vocabulary_graph: Broadcast,
        errors: Accumulator,
        warnings: Accumulator,
        lookup: Broadcast = None,
        ontology: Broadcast = None,
    ) -> None:
        g = generate_rdf_graph(
            rows,
            dataset,
            Namespace(namespace_url),
            vocabulary_mappings,
            vocabulary_graph,
            errors,
            warnings,
        )
        post_transform(g, ontology.value, table_name)
