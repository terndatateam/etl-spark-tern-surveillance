import glob
import itertools
import os

from pyspark import Accumulator, Broadcast
from pyspark.sql import SparkSession
from pyspark.sql.functions import col, lower, regexp_replace, substring, trim, when
from rdflib import Namespace
from spark_etl_utils import Transform
from spark_etl_utils.database import get_db_query_pg, get_postgres_url, save_dataframe_as_jdbc_table
from spark_etl_utils.rdf import generate_rdf_graph

from config import Config
from transform_tables.common import post_transform


class Table(Transform):
    def __init__(self, spark: SparkSession):
        super().__init__()
        self.spark = spark
        self.table = "tern_derived_species_richness"
        self.dataset = Config.DATASET
        self.namespace = Config.DATASET_NAMESPACE

        query = """
          ( select distinct 
                pi.site_location_visit_id as site_visit_id, 
                sl.site_location_name as site_id,          
                sl.site_location_name as plant_comm_id,      
                slv.visit_start_date as default_datetime,  
                hd.veg_barcode, 
                hd.herbarium_determination 
            from ausplots.point_intercept pi 
                left join ausplots.herbarium_determination hd on pi.veg_barcode = hd.veg_barcode 
                join ausplots.site_location_visit slv on pi.site_location_visit_id = slv.site_location_visit_id 
                join ausplots.site_location sl on slv.site_location_id = sl.site_location_id 
            where pi.growth_form not in ('Fungus', 'Bryophyte') 
                and hd.herbarium_determination != 'Dead Tree/Shrub'
                and hd.herbarium_determination is not null
          ) q
        """

        self.df = get_db_query_pg(
            self.spark,
            self.db_host,
            self.db_port,
            self.db_name,
            query,
            self.db_username,
            self.db_password,
        )

        # self.df.show()

        # Debug purposes
        if Config.DEBUG:
            self.df = self.df.filter(
                self.df["site_visit_id"].isin(Config.DEBUG_SITE_LOCATION_VISIT_ID)
            )
            # self.df.show()

    def pre_transform(self, errors, warnings):
        unmatched_species = set()
        for infile in glob.glob(("unmatched_species*.txt")):
            with open(os.path.join(Config.APP_DIR, infile), "r") as fp:
                unmatched_species.update(set(fp.read().splitlines()))

        clean_df = (
            self.df.select(
                "site_visit_id",
                "veg_barcode",
                regexp_replace(trim(self.df["herbarium_determination"]), " +", " ").alias(
                    "herbarium_determination"
                ),
            )
            .withColumn(
                "herbarium_determination",
                when(
                    lower(substring(col("herbarium_determination"), 0, 5)) == "no id",
                    "No Identifier",
                ).otherwise(col("herbarium_determination")),
            )
            .withColumn(
                "herbarium_determination",
                when(
                    col("herbarium_determination").isin(unmatched_species), col("veg_barcode")
                ).otherwise(col("herbarium_determination")),
            )
            .distinct()
            .filter(col("herbarium_determination").isNotNull())
        )

        clean_df.createOrReplaceTempView("clean_df")
        df_derived = self.spark.sql(
            """select (row_number() over (order by 1)) as unique_id, 
                                            site_visit_id,
                                            count(*) as species_richness
                                        from clean_df
                                        group by site_visit_id"""
        )

        self.df = self.df.drop("herbarium_determination").drop("veg_barcode")
        df_derived = df_derived.join(self.df, "site_visit_id").distinct()

        df_derived = df_derived.select(
            "site_id",
            "site_visit_id",
            "plant_comm_id",
            "unique_id",
            "default_datetime",
            "species_richness",
        )

        save_dataframe_as_jdbc_table(
            df_derived,
            get_postgres_url(
                self.db_host, self.db_port, self.db_name, self.db_username, self.db_password
            ),
            self.dataset,
            self.table,
        )

        self.df = df_derived

    @staticmethod
    def transform(
        rows: itertools.chain,
        dataset: str,
        namespace_url: str,
        table_name: str,
        vocabulary_mappings: Broadcast,
        vocabulary_graph: Broadcast,
        errors: Accumulator,
        warnings: Accumulator,
        lookup: Broadcast = None,
        ontology: Broadcast = None,
    ) -> None:
        g = generate_rdf_graph(
            rows,
            dataset,
            Namespace(namespace_url),
            vocabulary_mappings,
            vocabulary_graph,
            errors,
            warnings,
        )
        post_transform(g, ontology.value, table_name)
