import itertools

import psycopg2 as pg
from psycopg2._psycopg import AsIs
from pyspark import Broadcast, Accumulator, Row
from pyspark.sql import SparkSession
from rdflib import Literal, Namespace, URIRef, XSD
from shapely.geometry import Polygon as ShapelyPolygon
from spark_etl_utils import Transform, add_error, utm_to_lat_long
from spark_etl_utils.database import get_db_query_pg
from spark_etl_utils.rdf import Site, Attribute
from spark_etl_utils.rdf.models import Concept, Dataset, generate_underscore_uri, Point, Polygon
from tern_rdf import TernRdf
from tern_rdf.namespace_bindings import GEOSPARQL, TERN

from config import Config
from transform_tables.common import post_transform


class Table(Transform):

    def __init__(self, spark: SparkSession) -> None:
        super().__init__()
        self.spark = spark
        self.table = 'site_location_point'
        self.dataset = Config.DATASET
        self.namespace = Config.DATASET_NAMESPACE

        query = """
          ( select 
                slp.*,
                sl.site_location_name as site_id,
                slp.id as unique_id,
                sl.site_location_id as site_attr_unique_id
            from ausplots.site_location_point slp
            join ausplots.site_location sl
                on slp.site_location_id = sl.site_location_id 
            where slp.longitude <> 'NaN' and slp.latitude <> 'NaN' and sl.site_location_id in (
                select sl.site_location_id 
                from ausplots.site_location sl  
            )
          ) q
        """

        self.df = get_db_query_pg(
            self.spark,
            self.db_host,
            self.db_port,
            self.db_name,
            query,
            self.db_username,
            self.db_password,
        )

        # self.df.show()

        # Debug purposes
        if Config.DEBUG:
            self.df = self.df.filter(self.df['site_location_id'].isin(Config.DEBUG_SITE_LOCATION_ID))
            self.df.show()

    def load_lookup(self):
        ufoi_db = {"host": self.db_host, "port": self.db_port, "dbname": "ultimate_feature_of_interest",
                   "user": self.db_username, "password": self.db_password}

        return self.spark.sparkContext.broadcast([ufoi_db])

    @staticmethod
    def transform(rows: itertools.chain, dataset: str, namespace_url: str, table_name: str,
                  vocabulary_mappings: Broadcast, vocabulary_graph: Broadcast, errors: Accumulator,
                  lookup: Broadcast = None, ontology: Broadcast = None) -> None:

        ns = Namespace(namespace_url)

        POINT = 'point'
        LATITUDE = 'latitude'
        LONGITUDE = 'longitude'

        ufoi_db = lookup.value[0]
        conn = pg.connect(host=ufoi_db["host"], port=ufoi_db["port"], dbname=ufoi_db["dbname"],
                          user=ufoi_db["user"], password=ufoi_db["password"])

        g = TernRdf.Graph([dataset.upper()])

        grouped_rows_by_site = dict()
        for row in rows:
            """
            Example result: 
            {
                "SAAEYB0001": {
                    "CENTRE": Row(
                        id=1367071,
                        site_location_id=60372,
                        point='CENTRE',
                        easting=717265.818,
                        northing=6184704.08,
                        latitude=-34.456358333333334,
                        longitude=137.36516944444443,
                        zone=53,
                        threedcq=6.038,
                        site_visit_id=57637,
                        site_id='SAAEYB0001',
                        default_datetime=datetime.datetime(2015, 10, 12, 15, 0, 12),
                        unique_id=1367071
                    ),
                    "N1": {
                        ...
                    },
                    "N2": {
                        ...
                    }
                },
                "WAANOK0005": {
                    ...
                }
            }
            """
            k = row["site_id"]
            site_points = grouped_rows_by_site.get(k, dict())
            site_points.update({row[POINT]: row})
            grouped_rows_by_site.update({k: site_points})

        for site_grouped in grouped_rows_by_site.items():
            site_id = site_grouped[0]
            site_uri = Site.generate_uri(ns, site_id)
            point_dict = site_grouped[1]
            if "SW" not in point_dict:
                add_error(errors, "Did not find SW Point in site_location_point table for SITE_LOCATION_ID={}, "
                                  "calculating centroid".format(site_id))

                # Calculate centroid
                E_latitudes = 0
                E_longitudes = 0
                for p in point_dict.values():
                    E_latitudes += p[LATITUDE]
                    E_longitudes += p[LONGITUDE]

                lat = E_latitudes / len(point_dict)
                long = E_longitudes / len(point_dict)

                point_uri = URIRef(generate_underscore_uri(ns))
                g.add((site_uri, GEOSPARQL.hasGeometry, point_uri))
                point = Point(
                    uri=point_uri,
                    as_wkt=Literal("POINT({} {})".format(round(long, 8), round(lat, 8)), datatype=GEOSPARQL.wktLiteral),
                    latitude=Literal(lat, datatype=XSD.double),
                    longitude=Literal(long, datatype=XSD.double),
                )
                g += point.g

            else:
                location_SW = point_dict.get("SW")
                lat = location_SW[LATITUDE]
                long = location_SW[LONGITUDE]

                site_location_id = location_SW["site_location_id"]
                zone = location_SW["zone"]
                if site_location_id in [61341, 61345, 60270, 60271, 60272]:
                    lat, long = utm_to_lat_long(zone, lat, long)

                point_uri = URIRef(generate_underscore_uri(ns))
                g.add((site_uri, GEOSPARQL.hasGeometry, point_uri))
                point = Point(
                    uri=point_uri,
                    as_wkt=Literal("POINT({} {})".format(round(long, 8), round(lat, 8)), datatype=GEOSPARQL.wktLiteral),
                    latitude=Literal(lat, datatype=XSD.double),
                    longitude=Literal(long, datatype=XSD.double),
                )
                g += point.g

                if "NW" not in point_dict:
                    add_error(errors, "Did not find NW Point in site_location_point table for "
                                      "SITE_LOCATION_ID={}".format(site_grouped[0]))
                elif "NE" not in point_dict:
                    add_error(errors, "Did not find NE Point in site_location_point table for "
                                      "SITE_LOCATION_ID={}".format(site_grouped[0]))
                elif "SE" not in point_dict:
                    add_error(errors, "Did not find SE Point in site_location_point table for "
                                      "SITE_LOCATION_ID={}".format(site_grouped[0]))
                else:
                    polygon = "POLYGON(("
                    point_SW = point_dict.get("SW")
                    sw_lat = point_SW[LATITUDE]
                    sw_lon = point_SW[LONGITUDE]
                    if site_location_id in (61341, 61345, 60270, 60271, 60272):
                        sw_lat, sw_lon = utm_to_lat_long(zone, sw_lat, sw_lon)
                    polygon += str(round(sw_lon, 8)) + " " + str(round(sw_lat, 8)) + ", "
                    point_NW = point_dict.get("NW")
                    nw_lat = point_NW[LATITUDE]
                    nw_lon = point_NW[LONGITUDE]
                    if site_location_id in (61341, 61345, 60270, 60271, 60272):
                        nw_lat, nw_lon = utm_to_lat_long(zone, nw_lat, nw_lon)
                    polygon += str(round(nw_lon, 8)) + " " + str(round(nw_lat, 8)) + ", "
                    point_NE = point_dict.get("NE")
                    ne_lat = point_NE[LATITUDE]
                    ne_lon = point_NE[LONGITUDE]
                    if site_location_id in (61341, 61345, 60270, 60271, 60272):
                        ne_lat, ne_lon = utm_to_lat_long(zone, ne_lat, ne_lon)
                    polygon += str(round(ne_lon, 8)) + " " + str(round(ne_lat, 8)) + ", "
                    point_SE = point_dict.get("SE")
                    se_lat = point_SE[LATITUDE]
                    se_lon = point_SE[LONGITUDE]
                    if site_location_id in (61341, 61345, 60270, 60271, 60272):
                        se_lat, se_lon = utm_to_lat_long(zone, se_lat, se_lon)
                    polygon += str(round(se_lon, 8)) + " " + str(round(se_lat, 8)) + ", "
                    polygon += str(round(sw_lon, 8)) + " " + str(round(sw_lat, 8))
                    polygon += "))"
                    polygon_shapely = [(sw_lon, sw_lat),
                                       (nw_lon, nw_lat),
                                       (ne_lon, ne_lat),
                                       (se_lon, se_lat),
                                       (sw_lon, sw_lat)]

                    if 'nan' not in polygon.lower():
                        if ShapelyPolygon(polygon_shapely).is_valid:
                            polygon_uri = URIRef(generate_underscore_uri(ns))
                            g.add((site_uri, TERN.polygon, polygon_uri))
                            polygon = Polygon(polygon_uri, as_wkt=Literal(polygon, datatype=GEOSPARQL.wktLiteral))
                            g += polygon.g
                        else:
                            add_error(errors, "Invalid site-polygon in site_location_point table for "
                                              "SITE_LOCATION_ID={}".format(site_grouped[0]))
                    else:
                        add_error(errors, "Some point has NaN value in site_location_point table for "
                                          "SITE_LOCATION_ID={}".format(site_grouped[0]))

            # Calculate regions from TERN PostGIS
            i = 1
            for ufoi_key in Config.ufoi_list.keys():
                src_cursor = conn.cursor("serverCursor")
                src_cursor.execute(
                    "select %(value)s from %(table_name)s where ST_Intersects(geom, ST_SetSRID(ST_MakePoint(%(lon)s, %(lat)s), 3577))",
                    {'table_name': AsIs(ufoi_key), 'value': AsIs(Config.ufoi_list[ufoi_key][1]), 'lon': long,
                     'lat': lat})
                result = src_cursor.fetchone()
                if result is not None:
                    region_parent = Config.ufoi_list[ufoi_key][0]
                    region_item = result[0]
                    g.add((site_uri,
                           GEOSPARQL.sfWithin,
                           URIRef("{}{}".format(region_parent, region_item))))
                    i += 1
                src_cursor.close()

        post_transform(g, ontology.value, table_name)
