import math
import os
import numpy as np
import psycopg2 as pg
from pydap.client import open_url
from psycopg2._psycopg import AsIs
from pyspark.sql.types import Row

from spark_etl_utils.database import (
    get_db_table_pandas,
)

DB_SCHEMA_TABLE = "climate_data.climate_data_postgis"

# Keep datasets information together
CLIMATE_DATASET = {
    "https://thredds.nci.org.au/thredds/dodsC/gh70/ANUClimate/v2-0/stable/month/rain": {
        "variable": "rain",
        "function": [("precipitation_annual_mean", "annual")],
    },
    "https://thredds.nci.org.au/thredds/dodsC/gh70/ANUClimate/v2-0/stable/month/tavg": {
        "variable": "tavg",
        "function": [("temperature_annual_mean", "avg")],
    },
    "https://thredds.nci.org.au/thredds/dodsC/gh70/ANUClimate/v2-0/stable/month/tmax": {
        "variable": "tmax",
        "function": [("temperature_max", "max")],
    },
    "https://thredds.nci.org.au/thredds/dodsC/gh70/ANUClimate/v2-0/stable/month/tmin": {
        "variable": "tmin",
        "function": [("temperature_min", "min")],
    },
    "https://thredds.nci.org.au/thredds/dodsC/gh70/ANUClimate/v2-0/stable/month/srad": {
        "variable": "srad",
        "function": [("solar_radiation_mean", "avg")],
    },
}

CLIAMTE_VARIABLES = [
    "rain",
    "tavg",
    "tmax",
    "tmin",
    "srad",
]


def clean_value(value):
    if value == -9999 or value == -999 or math.isnan(value):
        return None
    else:
        return value


def pre_transform():
    # open opendap datasets
    datasets = {}
    for url in CLIMATE_DATASET.keys():
        # open dataset
        print("Retrieving data from: " + url)
        ds = open_url(url, user_charset="utf-8")

        # fetch index arrays and get min/max
        lats = ds["lat"][:].data
        lons = ds["lon"][:].data

        lat_min = lats[0]
        lat_max = lats[-1]
        if lat_min > lat_max:
            lat_min, lat_max = lat_max, lat_min
        lon_min = lons[0]
        lon_max = lons[-1]
        if lon_min > lon_max:
            lon_min, lon_max = lon_max, lon_min

        lat_increment = 0.01
        lon_increment = 0.01

        for vars in CLIMATE_DATASET[url]["function"]:
            # get grid data
            variable = CLIMATE_DATASET[url]["variable"]
            grid_data = ds[variable][variable]

            # store data
            datasets[variable] = {
                "ds": ds,
                "data": grid_data,
                "var": variable,
                "lats": lats,
                "lons": lons,
                "lon_min": lon_min,
                "lon_max": lon_max,
                "lat_min": lat_min,
                "lat_max": lat_max,
                "column_name": vars[0],
                "function": vars[1],
            }

    db_host = os.getenv("POSTGRES_HOST")
    db_port = os.getenv("POSTGRES_PORT")
    db_name = os.getenv("POSTGRES_DB")
    db_username = os.getenv("POSTGRES_USER")
    db_password = os.getenv("POSTGRES_PASSWORD")

    query = """
        select 
            s."ID" as site_id,
            CAST(coalesce(l."COLLECTEDSTARTLONGITUDE", l."COLLECTEDENDLONGITUDE", l."COLLECTEDMIDLONGITUDE") AS FLOAT8) as longitude,
            cast(coalesce(l."COLLECTEDSTARTLATITUDE", l."COLLECTEDENDLATITUDE", l."COLLECTEDMIDLATITUDE") AS FLOAT8) as latitude
        from qbeis."site" s 
            join qbeis."location" l on s."LOCATIONID" = l."ID"
            join qbeis.sitevisit sv on s."ID" = sv."SITEID"
        where sv."RECORDDATE" > '1960-01-01'
    """

    veg_pd_df = get_db_table_pandas(
        db_host,
        db_port,
        db_name,
        "test",
        db_username,
        db_password,
        "test",
        query=query,
    )

    conn = pg.connect(
        host=db_host,
        port=db_port,
        dbname=db_name,
        user=db_username,
        password=db_password,
    )
    new_rows = []
    # for row in self.df.rdd.collect():
    for _, row in veg_pd_df.iterrows():
        lat = row["latitude"]
        lon = row["longitude"]
        # print(row["site_id"])
        cursor = conn.cursor()
        cursor.execute(
            "SELECT * FROM %(table_name)s WHERE ST_Intersects(geom, ST_SetSRID(ST_MakePoint(%(lon)s, %(lat)s), 3577))",
            {"table_name": AsIs(DB_SCHEMA_TABLE), "lon": lon, "lat": lat},
        )
        result = cursor.fetchone()
        # cursor.close()
        if result:
            # new_rows.append(
            #     Row(
            #         **row.asDict(),
            #         precipitation_annual_mean=clean_value(result[2]),
            #         temperature_annual_mean=clean_value(result[3]),
            #         temperature_max=clean_value(result[4]),
            #         temperature_min=clean_value(result[5]),
            #         solar_radiation=clean_value(result[6]),
            #     )
            # )
            # print("found")
            # print(new_rows)
            pass
        else:
            # add_error(
            #     warnings,
            #     "Did not find CLIMATE DATA in {} table for coordinates lat='{}', lon='{}' of SITE_LOCATION_ID={}".format(
            #         DB_SCHEMA_TABLE, lat, lon, row["site_id"]
            #     ),
            # )
            try:
                lat_idx = int(np.abs(lats - lat).argmin())
                lon_idx = int(np.abs(lons - lon).argmin())

                # Calculate postGIS geometry (POLYGON)
                nw = "{} {}".format(
                    lons[lon_idx] - lon_increment / 2, lats[lat_idx] + lat_increment / 2
                )
                ne = "{} {}".format(
                    lons[lon_idx] + lon_increment / 2, lats[lat_idx] + lat_increment / 2
                )
                se = "{} {}".format(
                    lons[lon_idx] + lon_increment / 2, lats[lat_idx] - lat_increment / 2
                )
                sw = "{} {}".format(
                    lons[lon_idx] - lon_increment / 2, lats[lat_idx] - lat_increment / 2
                )
                polygon = "POLYGON(({nw}, {ne}, {se}, {sw}, {nw}))".format(
                    nw=nw, ne=ne, se=se, sw=sw
                )
                insert_params = {"table_name": AsIs(DB_SCHEMA_TABLE), "geom": polygon}

                # extract data
                for var in CLIAMTE_VARIABLES:
                    value = datasets[var]["data"][:, lat_idx, lon_idx].data
                    if datasets[var]["function"] == "annual":
                        insert_params.update(
                            {datasets[var]["column_name"]: np.average(value).item() * 12}
                        )
                    if datasets[var]["function"] == "avg":
                        insert_params.update(
                            {datasets[var]["column_name"]: np.average(value).item()}
                        )
                    elif datasets[var]["function"] == "max":
                        insert_params.update({datasets[var]["column_name"]: np.max(value).item()})
                    elif datasets[var]["function"] == "max_monthly":
                        reshape = np.reshape(value, (int(value.shape[0] / 12), 12))
                        insert_params.update(
                            {
                                datasets[var]["column_name"]: np.squeeze(np.max(reshape, axis=0))
                                .astype(float)
                                .tolist()
                            }
                        )
                    elif datasets[var]["function"] == "min":
                        insert_params.update({datasets[var]["column_name"]: np.min(value).item()})
                    elif datasets[var]["function"] == "min_monthly":
                        reshape = np.reshape(value, (int(value.shape[0] / 12), 12))
                        insert_params.update(
                            {
                                datasets[var]["column_name"]: np.squeeze(np.min(reshape, axis=0))
                                .astype(float)
                                .tolist()
                            }
                        )
                    elif datasets[var]["function"] == "timeless":
                        insert_params.update({datasets[var]["column_name"]: value.item()})
                    elif datasets[var]["function"] == "avg_monthly":
                        reshape = np.reshape(value, (int(value.shape[0] / 12), 12))
                        insert_params.update(
                            {
                                datasets[var]["column_name"]: np.squeeze(
                                    np.average(reshape, axis=0)
                                )
                                .astype(float)
                                .tolist()
                            }
                        )
                print(insert_params)
                cursor.execute(
                    """
                    INSERT INTO %(table_name)s (geom, precipitation_annual_mean, temperature_annual_mean, temperature_max, temperature_min, solar_radiation_mean)
                    VALUES(ST_GeomFromText(%(geom)s,3577), %(precipitation_annual_mean)s, %(temperature_annual_mean)s, %(temperature_max)s, %(temperature_min)s, %(solar_radiation_mean)s);
                    """,
                    insert_params,
                )
                conn.commit()
            except Exception as e:
                print(e)
                print(row["site_id"])
        cursor.close()
    conn.close()


if __name__ == "__main__":
    pre_transform()
