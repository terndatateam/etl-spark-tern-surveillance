import itertools
import random
import string

from pyspark import Accumulator, Broadcast
from pyspark.sql import SparkSession
from pyspark.sql.functions import col, udf, when
from pyspark.sql.types import StringType
from rdflib.namespace import Namespace
from spark_etl_utils import Transform, clean_string
from spark_etl_utils.database import get_db_query_pg
from spark_etl_utils.rdf import generate_rdf_graph

from config import Config
from transform_tables.common import post_transform


def clean_sample_id(val):
    alphabet = string.ascii_lowercase + string.digits
    if val in ("", "NA", "-", "None", "N/C", "NC"):
        return "".join(random.choices(alphabet, k=8))
    else:
        return val


class Table(Transform):
    def __init__(self, spark: SparkSession):
        super().__init__()
        self.spark = spark
        self.table = "soil_characterisation"
        self.dataset = Config.DATASET
        self.namespace = Config.DATASET_NAMESPACE

        debug_q = (
            f"""and sc.site_location_visit_id in ({",".join(Config.DEBUG_SITE_LOCATION_VISIT_ID_STR)})"""
            if Config.DEBUG
            else ""
        )
        query = f"""
          ( select 
                sc.*, 
                slv.soil_observation_type, 
                slv.site_location_visit_id as site_visit_id, 
                sl.site_location_name as site_id,
                slv.visit_start_date as default_datetime,  
                sc.id as soil_char_id,
                sc.layer_barcode as sample_id,
                sc.id as unique_id
            from ausplots.soil_characterisation sc 
                join ausplots.site_location_visit slv on sc.site_location_visit_id = slv.site_location_visit_id 
                join ausplots.site_location sl on slv.site_location_id = sl.site_location_id
            where sc.layer_barcode is not null and sc.layer_barcode <> '-'
            {debug_q}
          ) q
        """

        self.df = get_db_query_pg(
            self.spark,
            self.db_host,
            self.db_port,
            self.db_name,
            query,
            self.db_username,
            self.db_password,
        )

        # self.df.show()

        # Debug purposes
        # if Config.DEBUG:
        #     self.df = self.df.filter(self.df['site_visit_id'].isin(Config.DEBUG_SITE_LOCATION_VISIT_ID))

    def pre_transform(self, errors, warnings) -> None:
        clean_udf = udf(lambda val: clean_sample_id(val), StringType())
        self.df = (
            self.df.withColumn("sample_id", clean_udf("sample_id"))
            .withColumn("layer_barcode", when(col("layer_barcode") == "", None).otherwise(col("layer_barcode")))
            .withColumn("layer_barcode", when(col("layer_barcode") == "NA", None).otherwise(col("layer_barcode")))
            .withColumn("layer_barcode", when(col("layer_barcode") == "-", None).otherwise(col("layer_barcode")))
            .withColumn("layer_barcode", when(col("layer_barcode") == "None", None).otherwise(col("layer_barcode")))
            .withColumn("layer_barcode", when(col("layer_barcode") == "N/C", None).otherwise(col("layer_barcode")))
            .withColumn("layer_barcode", when(col("layer_barcode") == "NC", None).otherwise(col("layer_barcode")))
        )

        # self.df.show()

        # The goal of this pre_transform is cleaning data (NC means Not Collected)
        decode_udf = udf(lambda val: clean_string(val), StringType())
        self.df = (
            self.df.replace("NC", None)
            .withColumn("sample_id", decode_udf("sample_id"))
            .withColumn("smallest_size_1", when(col("smallest_size_1") == 11, None).otherwise(col("smallest_size_1")))
            .withColumn("smallest_size_2", when(col("smallest_size_2") == 11, None).otherwise(col("smallest_size_2")))
            .withColumn("next_size_1", when(col("next_size_1") == 11, None).otherwise(col("next_size_1")))
            .withColumn("next_size_2", when(col("next_size_2") == 11, None).otherwise(col("next_size_2")))
            .withColumn("smallest_size_1", when(col("smallest_size_1") == 10, "n/a").otherwise(col("smallest_size_1")))
            .withColumn("smallest_size_2", when(col("smallest_size_2") == 10, "n/a").otherwise(col("smallest_size_2")))
            .withColumn("next_size_1", when(col("next_size_1") == 10, "n/a").otherwise(col("next_size_1")))
            .withColumn("next_size_2", when(col("next_size_2") == 10, "n/a").otherwise(col("next_size_2")))
            .withColumn("texture_grade", when(col("texture_grade") == "NA", None).otherwise(col("texture_grade")))
        )

        # self.df.show()

    @staticmethod
    def transform(
        rows: itertools.chain,
        dataset: str,
        namespace_url: str,
        table_name: str,
        vocabulary_mappings: Broadcast,
        vocabulary_graph: Broadcast,
        errors: Accumulator,
        warnings: Accumulator,
        lookup: Broadcast = None,
        ontology: Broadcast = None,
    ) -> None:
        g = generate_rdf_graph(
            rows,
            dataset,
            Namespace(namespace_url),
            vocabulary_mappings,
            vocabulary_graph,
            errors,
            warnings,
        )
        post_transform(g, ontology.value, table_name)
