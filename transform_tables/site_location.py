import itertools

# import requests
from pyspark import Accumulator, Broadcast
from pyspark.sql import SparkSession
from pyspark.sql.functions import col, regexp_replace, udf
from pyspark.sql.types import (
    BooleanType,
    DoubleType,
    FloatType,
    IntegerType,
    StringType,
    StructField,
    StructType,
    TimestampType,
)
from rdflib import XSD, Literal, Namespace, URIRef
from spark_etl_utils import Transform, add_error
from spark_etl_utils.database import (
    get_db_query_pg,
    get_db_table_pandas,
    get_postgres_url,
    save_dataframe_as_jdbc_table,
)
from spark_etl_utils.rdf import (
    SITE_LOCATION_ID,
    SITE_LOCATION_VISIT_ID,
    SITE_TYPE_SITE,
    generate_rdf_graph,
)
from spark_etl_utils.rdf.models import RDFDataset, Site, SiteVisit

from config import Config
from transform_tables.common import post_transform

TERN_SITE_SCHEMA = StructType(
    [
        StructField("site_location_id", IntegerType(), True),
        StructField("site_location_name", StringType(), True),
        StructField("established_date", TimestampType(), True),
        StructField("description", StringType(), True),
        StructField("bioregion_name", StringType(), True),
        StructField("property", StringType(), True),
        StructField("paddock", StringType(), True),
        StructField("plot_is_permanently_marked", BooleanType(), True),
        StructField("plot_is_aligned_to_grid", BooleanType(), True),
        StructField("plot_is_100m_by_100m", BooleanType(), True),
        StructField("landform_pattern", StringType(), True),
        StructField("landform_element", StringType(), True),
        StructField("site_slope", StringType(), True),
        StructField("site_aspect", StringType(), True),
        StructField("plot_dimensions", StringType(), True),
        StructField("comments", StringType(), True),
        StructField("app_lat", StringType(), True),
        StructField("app_long", StringType(), True),
        StructField("outcrop_lithology", StringType(), True),
        StructField("other_outcrop_lithology", StringType(), True),
        StructField("surface_strew_size", IntegerType(), True),
        StructField("surface_strew_lithology", StringType(), True),
        StructField("site_visit_id", IntegerType(), True),
        StructField("site_id", StringType(), True),
        StructField("landform_id", StringType(), True),
        StructField("land_surface_id", StringType(), True),
        StructField("default_datetime", TimestampType(), True),
        StructField("unique_id", IntegerType(), True),
        StructField("site_attr_unique_id", IntegerType(), True),
        StructField("latitude", FloatType(), True),
        StructField("longitude", FloatType(), True),
        StructField("plot_width", DoubleType(), True),
        StructField("plot_length", DoubleType(), True),
        StructField("plot_area", DoubleType(), True),
        StructField("plot_shape", StringType(), True),
    ]
)

PLOT_SHAPE_SQUARE = "http://linked.data.gov.au/def/tern-cv/b2e07a7f-942e-44d9-9fd6-f0cee7be103d"
PLOT_SHAPE_RECTANGLE = "http://linked.data.gov.au/def/tern-cv/c4215f3c-1f25-4c1c-b910-27027848fc8f"


def get_plot_dimensions(dimensions):
    if dimensions in (
        "100 x 100m.",
        "'100  x  100'",
        "100m x 100m.",
        "100 X 100m.",
        "100 x 100m",
        "100 x 100",
        "100m. x 100m.",
        "100  x  100 m",
        "100 x 100 m.",
    ):
        return 100.0, 100.0, 100.0 * 100.0, PLOT_SHAPE_SQUARE
    elif dimensions in ("'200  x  50m.'", "200 m. x 50 m.", "200  x  50 m.", "'200  x  50 m.'"):
        return 50.0, 200.0, 50.0 * 200.0, PLOT_SHAPE_RECTANGLE
    elif dimensions == "100 x 1600m.":
        return 100.0, 1600.0, 100.0 * 1600.0, PLOT_SHAPE_RECTANGLE
    else:
        return 100.0, 100.0, 100.0 * 100.0, PLOT_SHAPE_SQUARE


class Table(Transform):
    def __init__(self, spark: SparkSession) -> None:
        super().__init__()
        self.spark = spark
        self.table = "site_location"
        self.dataset = Config.DATASET
        self.namespace = Config.DATASET_NAMESPACE

        query = """
          (select 
                sl.*,
                slv.site_location_visit_id as site_visit_id, 
                sl.site_location_name as site_id,
                sl.site_location_name as landform_id,
                sl.site_location_name as land_surface_id,
                slv.visit_start_date as default_datetime,  
                slv.site_location_visit_id as unique_id,
                sl.site_location_id as site_attr_unique_id
            from ausplots.site_location sl 
                join ausplots.site_location_visit slv on sl.site_location_id = slv.site_location_id
          ) q
        """

        self.df = get_db_query_pg(
            self.spark,
            self.db_host,
            self.db_port,
            self.db_name,
            query,
            self.db_username,
            self.db_password,
        )

        # self.df.show()

        # Debug purposes
        if Config.DEBUG:
            self.df = self.df.filter(
                self.df["site_location_id"].isin(Config.DEBUG_SITE_LOCATION_ID)
            )
            # self.df.show()

    def pre_transform(self, errors, warnings) -> None:
        self.table = "tern_{}".format(self.table)
        get_plot_dimensions_width_udf = udf(lambda z: get_plot_dimensions(z)[0], FloatType())
        get_plot_dimensions_length_udf = udf(lambda z: get_plot_dimensions(z)[1], FloatType())
        get_plot_dimensions_area_udf = udf(lambda z: get_plot_dimensions(z)[2], FloatType())
        get_plot_dimensions_shape_udf = udf(lambda z: get_plot_dimensions(z)[3], StringType())
        df_derived = self.df.withColumn(
            "plot_width", get_plot_dimensions_width_udf("plot_dimensions")
        )
        df_derived = df_derived.withColumn(
            "plot_length", get_plot_dimensions_length_udf("plot_dimensions")
        )
        df_derived = df_derived.withColumn(
            "plot_area", get_plot_dimensions_area_udf("plot_dimensions")
        )
        df_derived = df_derived.withColumn(
            "plot_shape", get_plot_dimensions_shape_udf("plot_dimensions")
        )
        df_derived = df_derived.withColumn(
            "surface_strew_size", regexp_replace(col("surface_strew_size"), "10", "NC")
        )

        self.df = df_derived
        save_dataframe_as_jdbc_table(
            self.df,
            get_postgres_url(
                self.db_host, self.db_port, self.db_name, self.db_username, self.db_password
            ),
            self.dataset,
            self.table,
        )

    def load_lookup(self):
        dataset_version = get_db_table_pandas(
            self.db_host,
            self.db_port,
            self.db_name,
            "r_ecoplatform_metadata",
            self.db_username,
            self.db_password,
            Config.DATASET,
            query="select load_date from {}.r_ecoplatform_metadata".format(Config.DATASET),
        ).iloc[0, 0]

        return self.spark.sparkContext.broadcast([dataset_version])

    @staticmethod
    def transform(
        rows: itertools.chain,
        dataset: str,
        namespace_url: str,
        table_name: str,
        vocabulary_mappings: Broadcast,
        vocabulary_graph: Broadcast,
        errors: Accumulator,
        warnings: Accumulator,
        lookup: Broadcast = None,
        ontology: Broadcast = None,
    ) -> None:
        # In order to loop over the "rows" iterator 2 times, we need to create a copy (as the iterator
        # is exhausted after the first loop).
        rows, rows_cloned = itertools.tee(rows)
        g = generate_rdf_graph(
            rows,
            dataset,
            Namespace(namespace_url),
            vocabulary_mappings,
            vocabulary_graph,
            errors,
            warnings,
        )
        dataset_uri = RDFDataset.generate_uri(Namespace(namespace_url))

        g += RDFDataset(
            uri=dataset_uri,
            title=Literal("TERN Surveillance Monitoring"),
            description=Literal(
                "<p> AusPlots is a collection of ecological data and samples gathered from a network"
                " of plots and transects across Australia by the TERN Ecosystem Surveillance team, "
                "using standardised methodologies. </p> <p>The AusPlots collection provides the "
                "ecological infrastructure to: </p> <ul><li>quantify the richness and cover of plant "
                "species (including weeds); </li><li>quantify the diversity and abundance of soil "
                "biodiversity; </li><li>assess the state, spatial heterogeneity and structural "
                "complexity of vegetation, including life-stage; </li><li>record vegetation and soil "
                "parameters that assist with the validation of remotely sensed ecological products;"
                "</li><li>analyse vegetation structure and change based on a series of photo reference "
                "images; </li><li>better estimate soil carbon and nutrient stocks; </li><li>conduct "
                "taxonomic validation studies based on collected plant voucher specimens; "
                "</li><li>conduct DNA barcoding and population genetic profiling based on collected "
                "tissue samples. </li></ul> <p> Overall this information will progress understanding "
                "of ecosystem processes, structure and function, and more generally progress "
                "understanding of the response to disturbance and longer-term environmental change of "
                "rangeland ecosystems, which underpins sustainable management practice.</p>."
            ),
            issued=Literal(lookup.value[0], datatype=XSD.date),
            # creator=URIRef("https://w3id.org/tern/resources/6be8678c-f354-47da-9667-46daf2a23ac7"),
            citation=Literal(
                "Sparrow, B. , Tokmakoff, A. , Leitch, E. , Guerin, G. , O'Neill, S. , McDonald, C. , Lowe, "
                "A. , Flitton, R. , Saleeba, T. , Coish, C. and TERN Surveillance (2021). TERN Surveillance "
                "monitoring program: Ecological Plot Survey Data and Samples collected from Field Sites "
                "across Australia. Terrestrial Ecosystem Research Network (TERN)."
            ),
            # publisher=URIRef(
            #     "https://w3id.org/tern/resources/a083902d-d821-41be-b663-1d7cb33eea66"
            # ),
        ).g
        for row in rows_cloned:
            site_uri = Site.generate_uri(Namespace(namespace_url), row[SITE_LOCATION_ID])
            site_visit_uri = SiteVisit.generate_uri(
                Namespace(namespace_url), row[SITE_LOCATION_VISIT_ID]
            )
            try:
                established_date = row["established_date"].strftime("%Y-%m-%d")
                g += Site(
                    uri=site_uri,
                    identifier=Literal(row[SITE_LOCATION_ID], datatype=XSD.string),
                    in_dataset=dataset_uri,
                    date_commissioned=Literal(established_date, datatype=XSD.date),
                    has_site_visit=site_visit_uri,
                    location_description=Literal(row["description"], datatype=XSD.string),
                    feature_type=URIRef(SITE_TYPE_SITE),
                ).g
            except Exception as e:
                print(e)
                add_error(
                    errors,
                    "Site establishment date missing or has wrong format for SITE_LOCATION_ID={}.".format(
                        row[SITE_LOCATION_ID]
                    ),
                )

        post_transform(g, ontology.value, table_name)

    # @staticmethod
    # def get_dataset_geonetwork(dataset_uuid):
    #     headers = {'accept': 'application/json'}
    #     r = requests.get(f"https://geonetwork-test.tern.org.au/geonetwork/srv/api/0.1/records/{dataset_uuid}", headers=headers)
    #     print(r.text)
