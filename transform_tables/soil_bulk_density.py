import itertools

from pyspark import Accumulator, Broadcast
from pyspark.sql import SparkSession
from pyspark.sql.functions import col, concat_ws, expr, udf
from pyspark.sql.types import FloatType, StringType
from rdflib import Namespace
from spark_etl_utils import Transform
from spark_etl_utils.database import get_db_query_pg, get_postgres_url, save_dataframe_as_jdbc_table
from spark_etl_utils.rdf import generate_rdf_graph

from config import Config
from transform_tables.common import post_transform


def format_depth(depth: str):
    # {'0': 1, '1': 1, '2': 1, '0.2-0.3': 1, '0.0-0.1': 1, '0.1-0.2': 1, '0.0-0.2': 1, '0.5-0.6': 1, '3': 1,
    #  '10-20cm': 1, '': 1, '20-30cm': 1, '': 1, '': 1, '': 1, '': 1}
    form = (
        depth.replace("m.", "m")
        .replace("cm.", "cm")
        .replace(" ", "")
        .replace("0.00", "0.0")
        .replace("0.10", "0.1")
        .replace("0.20", "0.2")
        .replace("0.30", "0.3")
        .replace("0.40", "0.4")
        .replace("0.50", "0.5")
        .replace("0.60", "0.6")
    )
    if form == "0" or form == "0.0-0.1" or form == "0-10cm" or form == "0-0.1m" or form == "0-0.1":
        return [0.0, 0.1, "0.0-0.1m"]
    elif form == "1" or form == "0.1-0.2" or form == "10-20cm" or form == "0.1-0.2m":
        return [0.1, 0.2, "0.1-0.2m"]
    elif form == "2" or form == "0.2-0.3" or form == "20-30cm" or form == "0.2-0.3m":
        return [0.2, 0.3, "0.2-0.3m"]
    elif form == "0.3-0.4":
        return [0.3, 0.4, "0.3-0.4m"]
    elif form == "0.4-0.5":
        return [0.4, 0.5, "0.4-0.5m"]
    elif form == "0.5-0.6":
        return [0.5, 0.6, "0.5-0.6m"]
    elif form == "0.0-0.2":
        return [0.0, 0.2, "0.0-0.2m"]
    else:
        return [0.0, 0.0, "0.0-0.0m"]


class Table(Transform):
    def __init__(self, spark: SparkSession):
        super().__init__()
        self.spark = spark
        self.table = "soil_bulk_density"
        self.dataset = Config.DATASET
        self.namespace = Config.DATASET_NAMESPACE

        query = """
          ( select 
                sbd.*,
                sbd.site_location_visit_id as site_visit_id, 
                sl.site_location_name as site_id,
                slv.visit_start_date as default_datetime,
                sbd.id as soil_bulk_id,
                sbd.id as unique_id
            from ausplots.soil_bulk_density sbd 
                join ausplots.site_location_visit slv on sbd.site_location_visit_id = slv.site_location_visit_id 
                join ausplots.site_location sl on slv.site_location_id = sl.site_location_id 
            where (sbd.fine_earth_bulk_density is null or sbd.fine_earth_bulk_density > 0) 
                and (sbd.gravel_bulk_density is null or sbd.gravel_bulk_density > 0) 
                and sbd.sample_id <> 'None'
          ) q
        """

        self.df = get_db_query_pg(
            self.spark,
            self.db_host,
            self.db_port,
            self.db_name,
            query,
            self.db_username,
            self.db_password,
        )

        # self.df.show()

        # Debug purposes
        if Config.DEBUG:
            self.df = self.df.filter(
                self.df["site_visit_id"].isin(Config.DEBUG_SITE_LOCATION_VISIT_ID)
            )
            # self.df.show()

    def pre_transform(self, errors, warnings):
        self.table = "tern_{}".format(self.table)
        format_depth_min_udf = udf(lambda z: format_depth(z)[0], FloatType())
        format_depth_max_udf = udf(lambda z: format_depth(z)[1], FloatType())
        format_depth_udf = udf(lambda z: format_depth(z)[2], StringType())
        df_derived = self.df.withColumn("soil_depth_min", format_depth_min_udf("sample_id"))
        df_derived = df_derived.withColumn("soil_depth_max", format_depth_max_udf("sample_id"))
        df_derived = df_derived.withColumn("soil_depth", format_depth_udf("sample_id"))
        df_derived = df_derived.withColumn(
            "wet_weight", expr("wet_weight_in_bag - paper_bag_weight")
        )
        df_derived = df_derived.withColumn(
            "oven_dried_weight", expr("oven_dried_weight_in_bag - paper_bag_weight")
        )
        df_derived = df_derived.withColumn(
            "sample_id", concat_ws("-", col("site_visit_id"), col("id"))
        )
        save_dataframe_as_jdbc_table(
            df_derived,
            get_postgres_url(
                self.db_host, self.db_port, self.db_name, self.db_username, self.db_password
            ),
            self.dataset,
            self.table,
        )
        self.df = df_derived

    @staticmethod
    def transform(
        rows: itertools.chain,
        dataset: str,
        namespace_url: str,
        table_name: str,
        vocabulary_mappings: Broadcast,
        vocabulary_graph: Broadcast,
        errors: Accumulator,
        warnings: Accumulator,
        lookup: Broadcast = None,
        ontology: Broadcast = None,
    ) -> None:
        g = generate_rdf_graph(
            rows,
            dataset,
            Namespace(namespace_url),
            vocabulary_mappings,
            vocabulary_graph,
            errors,
            warnings,
        )
        post_transform(g, ontology.value, table_name)
