import itertools

from pyspark import Accumulator, Broadcast
from pyspark.sql import SparkSession
from rdflib import XSD, Literal, Namespace, URIRef
from spark_etl_utils import Transform, add_error
from spark_etl_utils.database import get_db_query_pg
from spark_etl_utils.rdf import SITE_LOCATION_ID, SITE_LOCATION_VISIT_ID, generate_rdf_graph
from spark_etl_utils.rdf.models import RDFDataset, Site, SiteVisit

from config import Config
from transform_tables.common import post_transform


class Table(Transform):
    def __init__(self, spark: SparkSession) -> None:
        super().__init__()
        self.spark = spark
        self.table = "site_location_visit"
        self.dataset = Config.DATASET
        self.namespace = Config.DATASET_NAMESPACE

        query = """
          ( select 
                slv.*,
                slv.site_location_visit_id as site_visit_id, 
                sl.site_location_name as site_id,
                sl.site_location_name as weather_id,
                sl.site_location_name as plant_comm_id,
                sl.site_location_name as land_surface_id,
                sl.site_location_name as land_surface_disturbance_id,
                slv.visit_start_date as default_datetime,  
                slv.site_location_visit_id as unique_id
            from ausplots.site_location sl 
                join ausplots.site_location_visit slv on sl.site_location_id = slv.site_location_id
          ) q
        """

        self.df = get_db_query_pg(
            self.spark,
            self.db_host,
            self.db_port,
            self.db_name,
            query,
            self.db_username,
            self.db_password,
        )

        # self.df.show()

        # Debug purposes
        if Config.DEBUG:
            self.df = self.df.filter(
                self.df["site_location_visit_id"].isin(Config.DEBUG_SITE_LOCATION_VISIT_ID)
            )
            # self.df.show()

    @staticmethod
    def transform(
        rows: itertools.chain,
        dataset: str,
        namespace_url: str,
        table_name: str,
        vocabulary_mappings: Broadcast,
        vocabulary_graph: Broadcast,
        errors: Accumulator,
        warnings: Accumulator,
        lookup: Broadcast = None,
        ontology: Broadcast = None,
    ) -> None:
        # In order to loop over the "rows" iterator 2 times, we need to create a copy (as the iterator
        # is exhausted after the first loop).
        rows, rows_cloned = itertools.tee(rows)
        g = generate_rdf_graph(
            rows,
            dataset,
            Namespace(namespace_url),
            vocabulary_mappings,
            vocabulary_graph,
            errors,
            warnings,
        )

        dataset_uri = RDFDataset.generate_uri(Namespace(namespace_url))
        for row in rows_cloned:
            site_visit_uri = SiteVisit.generate_uri(
                Namespace(namespace_url), row[SITE_LOCATION_VISIT_ID]
            )
            try:
                start_date = row["visit_start_date"]
                end_date = row["visit_end_date"]
                g += SiteVisit(
                    uri=site_visit_uri,
                    identifier=Literal(start_date.strftime("%Y%m%d"), datatype=XSD.string),
                    in_dataset=dataset_uri,
                    has_site=Site.generate_uri(Namespace(namespace_url), row[SITE_LOCATION_ID]),
                    started_at_time=Literal(start_date, datatype=XSD.dateTime),
                    ended_at_time=Literal(end_date, datatype=XSD.dateTime),
                    location_description=Literal(row["location_description"], datatype=XSD.string),
                ).g
                g.add(
                    (
                        site_visit_uri,
                        URIRef("urn:ecoplots:published"),
                        Literal(row["ok_to_publish"], datatype=XSD.boolean),
                    )
                )
            except Exception as e:
                print(e)
                add_error(
                    errors,
                    "Site Visit dates missing or wrong format for SITE_LOCATION_VISIT_ID={}.".format(
                        row[SITE_LOCATION_VISIT_ID]
                    ),
                )

        post_transform(g, ontology.value, table_name)
