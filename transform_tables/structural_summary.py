import itertools

from pyspark import Accumulator, Broadcast
from pyspark.sql import SparkSession
from pyspark.sql.types import (
    BooleanType,
    IntegerType,
    Row,
    StringType,
    StructField,
    StructType,
    TimestampType,
)
from rdflib import Namespace
from spark_etl_utils import Transform
from spark_etl_utils.database import (
    get_db_query_pg,
    get_db_table_pandas,
    get_postgres_url,
    save_dataframe_as_jdbc_table,
)
from spark_etl_utils.rdf import generate_rdf_graph

from config import Config
from transform_tables.common import post_transform

TERN_SITE_SCHEMA = StructType(
    [
        StructField("id", IntegerType(), True),
        StructField("site_visit_id", IntegerType(), True),
        StructField("site_id", StringType(), True),
        StructField("default_datetime", TimestampType(), True),
        StructField("description", StringType(), True),
        StructField("unique_id", IntegerType(), True),
        StructField("mass_flowering_event", BooleanType(), True),
        StructField("phenology_comment", StringType(), True),
        StructField("plant_comm_id", IntegerType(), True),
        StructField("stratum", StringType(), True),
        StructField("most_dominant_species_name", StringType(), True),
        StructField("second_dominant_species_name", StringType(), True),
        StructField("third_dominant_species_name", StringType(), True),
    ]
)


def is_valid_value(value: str) -> bool:
    if value and value != "" and value != "None" and value != "-1":
        return True
    else:
        return False


class Table(Transform):
    def __init__(self, spark: SparkSession):
        super().__init__()
        self.spark = spark
        self.table = "structural_summary"
        self.dataset = Config.DATASET
        self.namespace = Config.DATASET_NAMESPACE

        query = """
          ( select 
                ss.*,
                ss.site_location_visit_id as site_visit_id, 
                sl.site_location_name as site_id,
                slv.visit_start_date as default_datetime,  
                sl.site_location_name as plant_comm_id,
                ss.id as unique_id
            from ausplots.structural_summary ss 
                join ausplots.site_location_visit slv on ss.site_location_visit_id = slv.site_location_visit_id 
                join ausplots.site_location sl on slv.site_location_id = sl.site_location_id 
            where id <> 70638
          ) q
        """
        # id=70638, visit=58752 is duplicated and useless

        self.df = get_db_query_pg(
            self.spark,
            self.db_host,
            self.db_port,
            self.db_name,
            query,
            self.db_username,
            self.db_password,
        )

        # self.df.show()

        # Debug purposes
        if Config.DEBUG:
            self.df = self.df.filter(
                self.df["site_visit_id"].isin(Config.DEBUG_SITE_LOCATION_VISIT_ID)
            )
            # self.df.show()

    def pre_transform(self, errors, warnings):
        self.table = "tern_{}".format(self.table)
        df_hd = get_db_table_pandas(
            self.db_host,
            self.db_port,
            self.db_name,
            "herbarium_determination",
            self.db_username,
            self.db_password,
            Config.DATASET,
            query="select veg_barcode, herbarium_determination from {}.herbarium_determination".format(
                Config.DATASET
            ),
        )

        new_rows = []
        for row in self.df.rdd.collect():
            strata = ["upper", "mid", "ground"]
            most_dominant_species_name = None
            second_dominant_species_name = None
            third_dominant_species_name = None
            for stratum in strata:
                col = "{}_1_dominant".format(stratum)
                if is_valid_value(row[col]):
                    vegetation = df_hd.query("veg_barcode == '{}'".format(row[col]))
                    if not vegetation.empty:
                        most_dominant_species_name = vegetation.iloc[0]["herbarium_determination"]
                col = "{}_2_dominant".format(stratum)
                if is_valid_value(row[col]):
                    vegetation = df_hd.query("veg_barcode == '{}'".format(row[col]))
                    if not vegetation.empty:
                        second_dominant_species_name = vegetation.iloc[0]["herbarium_determination"]
                col = "{}_3_dominant".format(stratum)
                if is_valid_value(row[col]):
                    vegetation = df_hd.query("veg_barcode == '{}'".format(row[col]))
                    if not vegetation.empty:
                        third_dominant_species_name = vegetation.iloc[0]["herbarium_determination"]

                if stratum == "upper":
                    stratum="U"
                elif stratum == "mid":
                    stratum = "M"
                else:
                    stratum="G"

                new_rows.append(
                    Row(
                        id=row["id"],
                        site_visit_id=row["site_visit_id"],
                        site_id=row["site_id"],
                        default_datetime=row["default_datetime"],
                        description=None,
                        unique_id="{}-{}".format(row["unique_id"], stratum),
                        mass_flowering_event=None,
                        phenology_comment=None,
                        plant_comm_id=None,
                        stratum=stratum,
                        most_dominant_species_name=most_dominant_species_name,
                        second_dominant_species_name=second_dominant_species_name,
                        third_dominant_species_name=third_dominant_species_name,
                    )
                )

            new_rows.append(
                Row(
                    id=row["id"],
                    site_visit_id=row["site_visit_id"],
                    site_id=row["site_id"],
                    default_datetime=row["default_datetime"],
                    description=row["description"],
                    unique_id=str(row["unique_id"]),
                    mass_flowering_event=row["mass_flowering_event"],
                    phenology_comment=row["phenology_comment"],
                    plant_comm_id=row["plant_comm_id"],
                    stratum=None,
                    most_dominant_species_name=None,
                    second_dominant_species_name=None,
                    third_dominant_species_name=None,
                )
            )

        self.df = self.spark.createDataFrame(new_rows)
        save_dataframe_as_jdbc_table(
            self.df,
            get_postgres_url(
                self.db_host, self.db_port, self.db_name, self.db_username, self.db_password
            ),
            self.dataset,
            self.table,
        )

    @staticmethod
    def transform(
        rows: itertools.chain,
        dataset: str,
        namespace_url: str,
        table_name: str,
        vocabulary_mappings: Broadcast,
        vocabulary_graph: Broadcast,
        errors: Accumulator,
        warnings: Accumulator,
        lookup: Broadcast = None,
        ontology: Broadcast = None,
    ) -> None:
        g = generate_rdf_graph(
            rows,
            dataset,
            Namespace(namespace_url),
            vocabulary_mappings,
            vocabulary_graph,
            errors,
            warnings,
        )
        post_transform(g, ontology.value, table_name)
