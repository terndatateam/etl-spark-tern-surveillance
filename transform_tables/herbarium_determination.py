import itertools
import os

from pyspark import Accumulator, Broadcast
from pyspark.sql import SparkSession
from pyspark.sql.functions import regexp_replace, udf
from pyspark.sql.types import StringType
from rdflib import Namespace
from spark_etl_utils import Transform, clean_string, trim
from spark_etl_utils.database import get_db_query_pg, get_postgres_url, save_dataframe_as_jdbc_table
from spark_etl_utils.rdf import generate_rdf_graph
from whoosh.index import open_dir

from config import Config
from transform_tables.common import create_apc_index, post_transform


class Table(Transform):
    def __init__(self, spark: SparkSession) -> None:
        super().__init__()
        self.spark = spark
        self.table = "herbarium_determination"
        self.dataset = Config.DATASET
        self.namespace = Config.DATASET_NAMESPACE
        self.matched_species = {}

        query = """
          ( select 
                hd.*,
                vv.default_growth_form as growth_form,
                vv.site_location_visit_id as site_visit_id, 
                sl.site_location_name as site_id,
                slv.visit_start_date as default_datetime,  
                hd.veg_barcode as sample_id,
                hd.veg_barcode as unique_id
            from ausplots.herbarium_determination hd 
                join ausplots.veg_vouchers vv on hd.veg_barcode = vv.veg_barcode 
                join ausplots.site_location_visit slv on vv.site_location_visit_id = slv.site_location_visit_id 
                join ausplots.site_location sl on slv.site_location_id = sl.site_location_id  
          ) q
        """

        self.df = get_db_query_pg(
            self.spark,
            self.db_host,
            self.db_port,
            self.db_name,
            query,
            self.db_username,
            self.db_password,
        )

        # self.df.show()

        # Debug purposes
        if Config.DEBUG:
            self.df = self.df.filter(
                self.df["site_visit_id"].isin(Config.DEBUG_SITE_LOCATION_VISIT_ID)
            )
            # self.df.show()

        # Create the index for the CSV source files.
        create_apc_index()
        # Load the index.
        self.ix = open_dir(os.path.join(Config.APP_DIR, Config.HERBARIUM_FILES, "index_files"))
        self.ix.writer()

    def load_lookup(self):
        return self.spark.sparkContext.broadcast(self.matched_species)

    def pre_transform(self, errors, warnings):
        decode_udf = udf(lambda val: clean_string(val), StringType())
        self.df = (
            self.df.withColumn(
                "herbarium_determination",
                regexp_replace(trim(self.df["herbarium_determination"]), " +", " ").alias(
                    "herbarium_determination"
                ),
            )
            .withColumn("veg_barcode", decode_udf("veg_barcode"))
            .withColumn("unique_id", decode_udf("unique_id"))
            .withColumn("sample_id", decode_udf("sample_id"))
        )

        self.table = "tern_{}".format(self.table)
        save_dataframe_as_jdbc_table(
            self.df,
            get_postgres_url(
                self.db_host, self.db_port, self.db_name, self.db_username, self.db_password
            ),
            self.dataset,
            self.table,
        )

    @staticmethod
    def transform(
        rows: itertools.chain,
        dataset: str,
        namespace_url: str,
        table_name: str,
        vocabulary_mappings: Broadcast,
        vocabulary_graph: Broadcast,
        errors: Accumulator,
        warnings: Accumulator,
        lookup: Broadcast = None,
        ontology: Broadcast = None,
    ) -> None:
        g = generate_rdf_graph(
            rows,
            dataset,
            Namespace(namespace_url),
            vocabulary_mappings,
            vocabulary_graph,
            errors,
            warnings,
        )

        post_transform(g, ontology.value, table_name)
